__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import os
import datetime

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    f = urllib2.urlopen(listpage,timeout=10)
    page_content = json.loads(f.read())
    return page_content

def getContentPage(content,siteid):

    article_id = content['id']
    article_title = content['title']
    # article_time = content['article_time']
    article_tags = ''
    article_time = time.strftime("%Y-%m-%d", time.localtime(content['publishTime']))
    article_url = content['articleUrl']
    pagecontent = getPage('{}&appVer=1&platform=youlu&udid=null&bundleId=cn.com.caijing.iphoneereader&magazines=caijing&devicemachine=null&network=wifi'.format(article_url))
    article_content = pagecontent['data']['content'].strip().replace('\n','').replace('\r','')
    article_author = pagecontent['data']['author']
    if content['thumbnails_url'] != None:
        article_cover = content['path']+content['thumbnails_url'][0]
    else:
        article_cover = ''
    detail_page = getPage('https://api.caijingmobile.com/ulu/article/detail/{}'.format(article_id))
    channel = []
    for column_name in detail_page['data']['column_list']:
        channel.append(column_name['column_name'].encode('utf-8'))
    channel = json.dumps(channel)

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover, channel)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover, channel):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover,channel)
    return article

def datemaker(input_date):
    date_list = []
    for i in range(0,100):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

if __name__=="__main__":

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_date = sys.argv[3]
    # input_date = '2018-01-01'
    completefile = str(sys.argv[1])+'.complete'
    lasttime = ''

    flag = 0
    status = 0
    initpage = 'http://api.caijingmobile.com/api/2.0/cms_articles.php?columnId=305&action=articlelist&lasttime={}' \
               '&appVer=1&platform=youlu&udid=null&bundleId=cn.com.caijing.iphoneereader&magazines=caijing' \
               '&devicemachine=null&network=wifi&pagesize=200'
    siteid = '5012'
    date_list = datemaker(input_date)

    while(True):
        listpage = initpage.format(lasttime)
        print listpage
        pagelist = getPage(listpage)
        # print len(pagelist['data']['items'])
        for item in pagelist['data']['articles']:
            publishtime = time.strftime("%Y-%m-%d", time.localtime(item['publishTime']))
            if publishtime in date_list:
                try:
                    article = getContentPage(item,siteid)
                    # print article
                    lasttime = item['publishTime']
                    # time.sleep(15)
                    writeDataSet(fileName, article)
                except:
                    error_record = str(item) + '\n'
                    writeDataSet(errorfile, error_record)
                    pass
                # flag = flag + 1
            if publishtime not in date_list:
                status = status + 1
            if status >= 3:
                break
        if status >= 3:
            break
    os.rename(fileName, completefile)
