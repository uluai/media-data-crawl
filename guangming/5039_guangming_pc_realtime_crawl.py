__author__ = 'elvis'
# -*- coding: utf-8 -*-

import traceback
import sys
import urllib2
import time
from bs4 import BeautifulSoup
import os
import json

reload(sys)
sys.setdefaultencoding('utf8')


def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def time_translator(t):
    timeArray = time.strptime(t, u"%Y-%m-%d")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp

def read_xml(node, siteid, url):
    if 'm.gmw.cn' not in node.find('link').text:
        article_tags = url.split('rss_')[1].replace('.xml', '')
        article_author = ''
        article_title = node.find('title').text
        article_url = node.find('link').text
        article_description = node.find('description').text
        article_id = article_url.split('content_')[1].split('.')[0]
        # t = '{}-{}'.format(article_url.split('/')[3], article_url.split('/')[4])
        # article_time = time_translator(t)
        article_content, article_time, article_cover, content_type = getContentPage(article_url)
        # article_time = time_translator(t)

        article = getArticleRecord(article_id, article_title, article_author, article_content, article_tags, article_time,
                                   siteid, article_url, article_cover, article_description, content_type)
        return article


def getContentPage(url):
    article_content = ''
    cover = []
    content_type = '0'
    f = urllib2.urlopen(url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    # print content
    if content.find_all('div', attrs={'class': 'ArticleContentBox'}):
        article_content_main = content.find_all('div', attrs={'class': 'ArticleContentBox'})
        c = article_content_main[0].find_all('p', attrs={'align': 'center'})
        for item in c:
            if 'img' in str(item):
                if 'content_logo' not in item.a.img.get('src'):
                    cover.append(item.a.img.get('src'))
        if 0 < len(cover) < 3:
            content_type = '1'
        if len(cover) > 2:
            content_type = '2'
        cover = json.dumps(cover)
        article_t = time_translator(content.find('span', attrs={'id': 'pubTime'}).text.split(' ')[0])
        for item in article_content_main[0].find_all('p'):
            article_content = '{}{}'.format(article_content,
                                            item.get_text().replace('\n', '').replace('\r', '').strip())
        return article_content, article_t, cover, content_type
    if content.find_all('div', attrs={'id': 'contentMain'}):
        article_content_main = content.find_all('div', attrs={'id': 'contentMain'})
        article_t = time_translator(content.find('span', attrs={'id': 'pubTime'}).text.split(' ')[0])
        a = article_content_main[0]
        b = a.find_all('p')
        for item in b:
            article_content = '{}{}'.format(article_content, item.get_text().replace('\n', '').replace('\r', '').strip())
        c = article_content_main[0].find_all('p', attrs={'align': 'center'})
        for item in c:
            # if 'img' in str(item) and 'imge.gmw.cn' in str(item):
            if 'img' in str(item):
                if 'content_logo' not in item.img.get('src'):
                    cover.append(item.img.get('src'))
        if 0 < len(cover) < 3:
            content_type = '1'
        if len(cover) > 2:
            content_type = '2'
        cover = json.dumps(cover)
        return article_content, article_t, cover, content_type


def getArticleRecord(id, title, editor, content, tags, publishtime, siteid, url, cover, description, content_type):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,
                                                                                  publishtime,siteid,url,cover,
                                                                                  description,content_type)
    return article

if __name__=="__main__":
    # print getContentPage('http://e.gmw.cn/2017-07/04/content_24975237.htm')
    pc_init_url = ['http://tech.gmw.cn/rss_tech.xml',
                   'http://politics.gmw.cn/rss_politics.xml',
                   'http://difang.gmw.cn/rss_difang.xml',
                   'http://e.gmw.cn/rss_e.xml',
                   'http://economy.gmw.cn/rss_economy.xml',
                   'http://guancha.gmw.cn/rss_guancha.xml',
                   'http://lady.gmw.cn/rss_lady.xml',
                   'http://yangsheng.gmw.cn/rss_yangsheng.xml']
    pc_siteid = '5039'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'
    for url in pc_init_url:
        # print url
        f = urllib2.urlopen(url, timeout=10)
        content = BeautifulSoup(f.read(), 'xml')
        for node in content.findAll('item'):
            try:
                article = read_xml(node, pc_siteid, url)
                # print article
                writeDataSet(fileName, article)
                # time.sleep(1)
            except Exception, e:
                # exstr = traceback.format_exc()
                # print exstr
                error_record = str(node)+'\n'
                # print error_record
                writeDataSet(errorfileName, error_record)
                pass
    os.rename(fileName, completefile)

