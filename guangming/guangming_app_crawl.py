__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib2
import json
import time
from bs4 import BeautifulSoup
import os


reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName, 'a')
    fr.write(data)

def read_json(node, siteid):
    article_url = node.get('artUrl')
    article = ''
    if 'shtml' in article_url:
        article_title = node.get('title')
        article_tags = node.get('masterId')

        article_cover = node.get('picLinks')
        article_time = node.get('pubTime')
        article_id = node.get('articleId')
        article_author = ''
        article_content = getContentPage(article_url)
        article = getArticleRecord(article_id, article_title, article_author, article_content, article_tags,
                                   article_time, siteid, article_url, article_cover)
    return article


def getContentPage(url):
    f = urllib2.urlopen(url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    article_content = content.body.find('div', attrs={'class': 'content_main'}).get_text().replace('\n','').replace('\r','').strip()
    return article_content


def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

if __name__=="__main__":
    app_init_url = ['https://s.cloud.gmw.cn/2016/json/sxw/rd/',
                    'https://s.cloud.gmw.cn/2016/json/sxw/wy/',
                    'https://s.cloud.gmw.cn/2016/json/sxw/gn/']
    app_siteid = '5041'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'

    for url in app_init_url:
        new_next_page = url
        exist_tag = 0
        while(exist_tag == 0):
            print new_next_page
            f = urllib2.urlopen(new_next_page, timeout=10)
            content = json.loads(f.read())
            next_page = content.get('nextPageUrl')
            if 'http' not in next_page:
                new_next_page = "{}{}".format(url, next_page)
            if 'http' in next_page:
                new_next_page = next_page
            if len(content.get('list')) > 0:
                for node in content.get('list'):
                    try:
                        article = read_json(node, app_siteid)
                        if article != '':
                            # print article
                            writeDataSet(fileName, article)
                            time.sleep(1)
                    except:
                        error_record = str(node)+'\n'
                        writeDataSet(errorfileName, error_record)
                        pass
            if len(content.get('list')) <= 0:
                exist_tag = 1
    os.rename(fileName, completefile)

