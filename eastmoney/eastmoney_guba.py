# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import hashlib
import re
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                             "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
               "Cookie": "st_pvi=42276008648835; st_si=60870070820405"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    # h = httplib2.Http(".cache")
    # resp, content = h.request(page, "GET")
    list_page_content = BeautifulSoup(list_page.read(), 'html.parser', from_encoding="gb18030")
    # print list_page_content
    result_list = list_page_content.findAll('div', attrs={'class': re.compile(r"^articleh")})
    # print result_list
    for item in result_list:
        # print item
        if item.find('span', attrs={'class': 'l3'}):
            url = 'http://guba.eastmoney.com{}'.format(item.find('span', attrs={'class': 'l3'}).a.get('href'))
            if '002638' in url:
                title = item.find('span', attrs={'class': 'l3'}).a.get('title')
                id = url.split(',')[2]
                if item.find('span', attrs={'class': 'l1'}):
                    page_view = item.find('span', attrs={'class': 'l1'}).text
                if item.find('span', attrs={'class': 'l2'}):
                    comments = item.find('span', attrs={'class': 'l2'}).text

                if item.find('span', attrs={'class': 'l4'}):
                    source = item.find('span', attrs={'class': 'l4'}).text
                    if item.find('span', attrs={'class': 'l4'}).find('a'):
                        source_url = item.find('span', attrs={'class': 'l4'}).a.get('href')
                if item.find('span', attrs={'class': 'l6'}):
                    source_time = item.find('span', attrs={'class': 'l6'}).text
                generate_result(id, title, url, source, source_url, source_time, page_view, comments)


def generate_result(id, title, url, source, source_url, source_time, page_view, comments):
    new_result = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}'.format(id, title, url, source, source_url, source_time, page_view, comments)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://guba.eastmoney.com/list,002638,f_1.html'
    # for p in range(1, 10):
    #     search_page = init_page.format(p)
    get_news_list(init_page)