# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import json
import time


reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)

def time_compiler(rare_time):
    rare_time = rare_time[:10].split('-')
    tup = (int(rare_time[0]), int(rare_time[1]), int(rare_time[2]), 0, 0, 0, 0, 0, 0)
    t = time.mktime(tup)
    complete_time = str(time.strftime("%Y%m%d", time.localtime(t)))
    return complete_time

def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                             "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
               "Cookie": "st_pvi=42276008648835; st_si=60870070820405"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    list_page_content = json.loads(list_page.read()[1:-1])
    for item in list_page_content:
        # print item
        if 'datetime' in item:
            source_time = item.get('datetime')
        if 'sratingName' in item:
            rating_category = item.get('sratingName')
        if 'change' in item:
            rating_change = item.get('change')
        if 'insName' in item:
            report_object = item.get('insName')
        if 'title' in item:
            report = item.get('title')
        if 'infoCode' in item:
            report_id = item.get('infoCode')
        if 'insCode' in item:
            company_code = item.get('insCode')
        report_url = 'http://data.eastmoney.com/report/{}/{}.html'.format(time_compiler(source_time), report_id)
        object_url = 'http://data.eastmoney.com/report/{}_0.html'.format(company_code)

        # print source_time,rating_category,rating_change,report_object,report,category,report_id,report_url,object_url
        generate_result(source_time, rating_category, rating_change, report_object,
                        report, report_id, report_url, object_url)
    #         security_code = ''
    #         title = ''
    #         source_time = ''
    #         column_name = ''
    #         column_code = ''
    #         if 'NOTICETITLE' in item:
    #             title = item.get('NOTICETITLE')
    #         if 'ANN_RELCOLUMNS' in item:
    #             column = item.get('ANN_RELCOLUMNS')
    #             if 'COLUMNNAME' in column and 'COLUMNCODE' in column:
    #                 column_name = column.get('COLUMNNAME')
    #                 column_code = column.get('COLUMNCODE')
    #         if 'NOTICEDATE' in item:
    #             source_time = item.get('NOTICEDATE')
    #         if 'INFOCODE' in item:
    #             notice_id = item.get('INFOCODE')
    #         if 'ANN_RELCODES' in item:
    #             security_info = item.get('ANN_RELCODES')[0]
    #             if 'SECURITYCODE' in security_info:
    #                 security_code = security_info.get('SECURITYCODE')
    #         pdf_url = 'http://pdf.dfcfw.com/pdf/H2_{}_1.pdf'.format(notice_id)
    #         url = 'http://data.eastmoney.com/notices/detail/{}/{},JUU1JThCJUE0JUU0JUI4JThBJUU1JTg1JTg5JUU3JTk0JUI1' \
    #               '.html'.format(security_code, notice_id)
    #         generate_result(notice_id, title, url, pdf_url, source_time, column_name, column_code)


def generate_result(source_time, rating_category,  rating_change, report_object,
                    report, report_id, report_url, object_url):
    new_result = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}'.format(
        report_id, source_time, rating_category,  rating_change, report_object, object_url, report, report_url)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://datainterface.eastmoney.com//EM_DataCenter/js.aspx?type=SR&sty=GGSR&ps=100&p={page}&code=002638'
    # for p in range(1, 10):
        # search_page = init_page.format(p)
    get_news_list(init_page)