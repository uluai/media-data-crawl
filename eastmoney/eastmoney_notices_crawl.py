# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import json

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                             "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
               "Cookie": "st_pvi=42276008648835; st_si=60870070820405"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    list_page_content = json.loads(list_page.read().decode('gb18030')[15:-1])
    # print list_page_content
    if 'data' in list_page_content:
        result_list = list_page_content.get('data')
        for item in result_list:
            security_code = ''
            title = ''
            source_time = ''
            column_name = ''
            column_code = ''
            if 'NOTICETITLE' in item:
                title = item.get('NOTICETITLE')
            if 'ANN_RELCOLUMNS' in item:
                column = item.get('ANN_RELCOLUMNS')
                if 'COLUMNNAME' in column and 'COLUMNCODE' in column:
                    column_name = column.get('COLUMNNAME')
                    column_code = column.get('COLUMNCODE')
            if 'NOTICEDATE' in item:
                source_time = item.get('NOTICEDATE')
            if 'INFOCODE' in item:
                notice_id = item.get('INFOCODE')
            if 'ANN_RELCODES' in item:
                security_info = item.get('ANN_RELCODES')[0]
                if 'SECURITYCODE' in security_info:
                    security_code = security_info.get('SECURITYCODE')
            pdf_url = 'http://pdf.dfcfw.com/pdf/H2_{}_1.pdf'.format(notice_id)
            url = 'http://data.eastmoney.com/notices/detail/{}/{},JUU1JThCJUE0JUU0JUI4JThBJUU1JTg1JTg5JUU3JTk0JUI1' \
                  '.html'.format(security_code, notice_id)
            generate_result(notice_id, title, url, pdf_url, source_time, column_name, column_code)


def generate_result(notice_id, title, url, pdf_url, source_time, column_name, column_code):
    new_result = '{}\001{}\001{}\001{}\001{}\001{}\001{}'.format(
        notice_id, title, url, pdf_url, source_time, column_name, column_code)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://data.eastmoney.com/notices/getdata.ashx?' \
                'StockCode=002638&CodeType=1&PageIndex=1&PageSize=50' \
                '&jsObj=SzodFdsK&SecNodeType=0&FirstNodeType=0&rt=49577625'
    # for p in range(1, 10):
        # search_page = init_page.format(p)
    get_news_list(init_page)