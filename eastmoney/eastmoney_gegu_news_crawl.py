# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import json
import hashlib

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                             "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
               "Cookie": "st_pvi=42276008648835; st_si=60870070820405"}
    request = urllib2.Request(page, headers=headers)
    # request = urllib2.Request(page)
    list_page = urllib2.urlopen(request)

    list_page_content = json.loads(list_page.read()[15:-1])
    if 'DataResult' in list_page_content:
        result_list = list_page_content.get('DataResult')
        for item in result_list:
            if 'Title' in item:
                title = item.get('Title')
            if 'Url' in item:
                url = item.get('Url')
            if 'Source' in item:
                source = item.get('Source')
            if 'ShowTime' in item:
                source_time = item.get('ShowTime')
            if 'Description' in item:
                summary = item.get('Description')
            generate_result(title, url, source, source_time, summary)


def generate_result(title, url, source, source_time, summary):
    myMd5 = hashlib.md5()
    myMd5.update(title)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}\001{}'.format(id, title, url, source, source_time, summary)
    print new_result


def write_data_to_kafka():
    pass


def write_data_to_file():
    pass


def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://so.eastmoney.com/Search.ashx?' \
                'qw=(002638)(%E5%8B%A4%E4%B8%8A%E5%85%89%E7%94%B5)(%E5%8B%A4%E4%B8%8A)&qt=2&sf=0&st=1&cpn=1&pn=10&f=0&p=0'
    # init_page = 'http://news.baidu.com/ns?word=勤上光电&pn={}&cl=2&ct=0&tn=news&rn=20&ie=utf-8&bt=0&et=0'
    get_news_list(init_page)
