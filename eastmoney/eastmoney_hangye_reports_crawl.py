# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import json
import time


reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)

def time_compiler(rare_time):
    rare_time = rare_time.split(' ')[0].split('/')
    tup = (int(rare_time[0]), int(rare_time[1]), int(rare_time[2]), 0, 0, 0, 0, 0, 0)
    t = time.mktime(tup)
    complete_time = str(time.strftime("%Y%m%d", time.localtime(t)))
    return complete_time

def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                             "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
               "Cookie": "st_pvi=42276008648835; st_si=60870070820405"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    list_page_content = json.loads(list_page.read()[1:-1])
    # print list_page_content['data']
    for item in list_page_content:
        # print item
        column = item.split(',')
        source_time = column[1]
        rating_category = column[7]
        rating_change = column[8]
        report_object = column[4]
        report = column[9]
        category = column[10]
        report_id = column[2]
        report_url = 'http://data.eastmoney.com/report/{}/hy,{}.html'.format(time_compiler(source_time), report_id)
        object_url = 'http://data.eastmoney.com/report/{}_0.html'.format(column[3])

        # print source_time,rating_category,rating_change,report_object,report,category,report_id,report_url,object_url
        generate_result(source_time, rating_category,  rating_change, report_object,
                        report, category, report_id, report_url, object_url)


def generate_result(source_time, rating_category,  rating_change, report_object,
                    report, category, report_id, report_url, object_url):
    new_result = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}'.format(
        report_id, source_time, rating_category,  rating_change, report_object, object_url, report, report_url, category)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://datainterface.eastmoney.com//EM_DataCenter/js.aspx?' \
                'type=SR&sty=HYSR&mkt=0&stat=0&cmd=4&code=459&sc=&ps=25&p=1&rt=49577836'
    # for p in range(1, 10):
        # search_page = init_page.format(p)
    get_news_list(init_page)