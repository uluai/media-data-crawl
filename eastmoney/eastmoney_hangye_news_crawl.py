# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import hashlib
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 Firefox/35.0", "Cookie": "BDUSS=AAAAAAAAAAAAAAAAAAAAAAAA"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    # h = httplib2.Http(".cache")
    # resp, content = h.request(page, "GET")
    list_page_content = BeautifulSoup(list_page.read(), 'html.parser', from_encoding="gb18030")
    result_list = list_page_content.find('div', attrs={'class': 'listBox'}).find('div', attrs={'class': 'list'}).\
        findAll('li')
    # print result_list
    for item in result_list:
        print item
        title = item.a.get('title')
        url = item.a.get('href')
        source = ''
        # source_url = news_info.a.get('href')
        source_time = item.span.text
        summary = ''
        generate_result(title, url, source, source_time, summary)


def generate_result(title, url, source, source_time, summary):
    myMd5 = hashlib.md5()
    myMd5.update(title)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}\001{}'.format(id, title, url, source, source_time, summary)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://stock.eastmoney.com/hangye/459_1.html'
    # for p in range(1, 10):
    #     search_page = init_page.format(p)
    get_news_list(init_page)