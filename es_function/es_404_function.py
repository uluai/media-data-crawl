# -*- coding:utf-8 -*-
__author__ = 'elvis'

# guangming 404 anto_delete function, input 2 parameters: index_name(eg:art-product), document_type(eg:5039)
# delete es record by date function, input 3 parameters: index_name(eg:art-product), document_type(eg:5039), input_date(eg:2017-08-22)

from elasticsearch import Elasticsearch
from elasticsearch import helpers
import urllib2
import sys
import time

class ESFunc(object):

    def __init__(self):
        self.es = Elasticsearch(
            ['10.0.0.4', '10.0.0.5', '10.0.0.6', '10.0.0.7'],
            port=9200,
        )

    def get_all_record(self, index, doc_type):
        query = {"query": {"match_all": {}}}
        res = helpers.scan(client=self.es, index=index, doc_type=doc_type, query=query, scroll="10m")
        return res

    def test(self):
        a = {'content_type': '0'}
        # self.es.update(index="art-product", doc_type="5047", id="30996890", body={'doc': a})
        res = self.es.get(index="art-product", doc_type="5047", id="30996890")
        return res

    def update(self, id, type_id):
        a = {'content_type': type_id}
        self.es.update(index="art-product", doc_type="5048", id=id, body={'doc': a})

    def delete_record(self, index, doc_type, record_id):
        self.es.delete(index=index, doc_type=doc_type, id=record_id)

    @staticmethod
    def check_404_status(url):
        # 'http://lady.gmw.cn/2017-08/02/content_253862651.htm'
        status_code = False
        response = None
        try:
            response = urllib2.urlopen(url, timeout=10)
        except urllib2.URLError as e:
            if hasattr(e, 'code'):
                if e.code == 404:
                    status_code = True
        finally:
            if response:
                response.close()
        return status_code

    @staticmethod
    def check_input_date(input_date):
        time_array = time.strptime(input_date, u"%Y-%m-%d")
        time_stamp = int(time.mktime(time_array))
        local_date = int(time.time())
        if (local_date - time_stamp) > 604800:
            return time_stamp
        else:
            print "Please check the input date."
            return 0

    def delete_record_by_date(self, index, doc_type, record_id, input_date, record_date):
        if record_date < input_date:
            # print record_id
            self.es.delete(index=index, doc_type=doc_type, id=record_id)

if __name__ == '__main__':
    if len(sys.argv) == 3:
        es_index = sys.argv[1]
        es_doc = sys.argv[2]
        es_function = ESFunc()
        # es_function.check_404_status('http://lady.gmw.cn/2017-08/02/content_253862651.htm')
        all_record = es_function.get_all_record(es_index, es_doc)
        for item in all_record:
            item_id = item['_id']
            # print item_id
            try:
                item_url = item['_source']['url']
                if es_function.check_404_status(item_url):
                    # es_function.delete_record(es_index, es_doc, item_id)
                    print 'Deleted item: id:{}, title:{}, url:{}'.format(item['_id'], item['_source']['title'],
                                                                        item['_source']['url'])
            except:
                pass

    if len(sys.argv) == 4:
        es_index = sys.argv[1]
        es_doc = sys.argv[2]
        es_delete_date = sys.argv[3]
        es_function = ESFunc()
        checked_input_date = es_function.check_input_date(es_delete_date)
        if checked_input_date != 0:
            all_record = es_function.get_all_record(es_index, es_doc)
            for item in all_record:
                item_id = item['_id']
                # print "lalala"+item_id
                try:
                    item_date = item['_source']['date']
                    es_function.delete_record_by_date(es_index, es_doc, item_id, checked_input_date, item_date)
                    print 'Deleted item: id:{}, title:{}, url:{}'.format(item['_id'], item['_source']['title'],
                                                                         item['_source']['url'])
                except:
                    pass