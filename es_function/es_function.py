# -*- coding:utf-8 -*-
__author__ = 'elvis'

from elasticsearch import Elasticsearch
from elasticsearch import helpers
import json
import urllib2
from bs4 import BeautifulSoup


class ESFunc(object):
    def __init__(self):
        self.es = Elasticsearch(
            ['10.0.0.4', '10.0.0.5', '10.0.0.6', '10.0.0.7'],
            port=9200,
        )

    def test1(self):
        query = {"query": {"match_all": {}}}
        res = helpers.scan(client=self.es, index="art-product", doc_type="7001", query=query, scroll="10m")
        return res

    def test(self):
        a = {'content_type': '0'}
        # self.es.update(index="art-product", doc_type="5047", id="30996890", body={'doc': a})
        res = self.es.get(index="art-product", doc_type="5047", id="30996890")
        return res

    def update(self, id, type_id, pic_array):
        a = {'content_type': type_id}
        b = {'pic': pic_array}
        # print str(b)
        self.es.update(index="art-product", doc_type="7001", id=id, body={'doc': a})
        self.es.update(index="art-product", doc_type="7001", id=id, body={'doc': b})
        # res = self.es.get(index="art-product", doc_type="5047", id="30996890")
        # def update(self, id, type_id):
        #     a = {'content_type': type_id}
        #     self.es.update(index="art-product", doc_type="5048", id=id, body={'doc': a})


def check_pic():
    t = ESFunc()
    # aaa = t.test1()
    # print aaa
    # for hit in aaa:
    #     print hit
    # print t.test1()
    test_result = t.test1()
    for item in test_result:
        new_array = []
        # print type(item)
        # print len(test_result['_source']['pic'])
        id = item['_id']
        print id
        try:
            pic = json.loads(item['_source']['pic'])
            for p in pic:
                print "p:" + p
                if 'http' in p:
                    print "find"
                    new_array.append(p)
                    print "new_array:" + str(new_array)
            content_type = '0'
            if 0 < len(new_array) < 3:
                content_type = "1"
            if len(new_array) > 2:
                content_type = "2"
            cover = json.dumps(new_array)
            t.update(id, content_type, cover)
        except:
            pass


# def addpic():
t = ESFunc()
test_result = t.test1()
for item in test_result:
    id = item['_id']
    # print id

    url = item['_source']['url']
    source = item['_source']['source']
    if source == 'sina':
        request = urllib2.Request(url.replace('.com', ''))
        f = urllib2.urlopen(request, timeout=10)
        article_content = BeautifulSoup(f.read(), 'html.parser')
        article_info = article_content.find('article', attrs={'class': 'art_box'})
        cover = []
        content_type = '0'
        try:
            article_img_list = article_info.find_all('img', attrs={'class': 'art_img_mini_img j_'
                                                                            'fullppt_cover'})
            # print 'article_img_list: {}'.format(str(article_img_list))
            if len(article_img_list) > 0:
                for article_img in article_img_list:
                    cover.append('https:{}'.format(article_img.get('data-src')))
                if 0 < len(cover) < 3:
                    content_type = '1'
                if len(cover) > 2:
                    content_type = '2'
        except:
            pass
        cover = json.dumps(cover)
        # print 'cover: {}, content_type: {}'.format(str(cover), content_type)
        t.update(id, content_type, cover)


