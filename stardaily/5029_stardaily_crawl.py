__author__ = 'elvis'
# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
import urllib2
import sys
import datetime
import time
import os

reload(sys)
sys.setdefaultencoding('utf8')

def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    print len(loaddata)
    return loaddata

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)
    # fr.close()

def stardaily_crawl_for_url_list(url):
    f = urllib2.urlopen(url, timeout=10)
    content_structure = BeautifulSoup(f.read(), 'html.parser')
    current_url = content_structure.find('li', attrs={'class': 'item ov'}).a.get('href')
    return current_url

def validate_content_url(url):
    status = 404
    try:
        f = urllib2.urlopen(url, timeout=10)
        status = f.code
    except urllib2.HTTPError, e:
        status = e.code
    return status

def datemaker(input_date):
    date_list = []
    for i in range(0,1000):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list


def stardailycrawlforcontent(url):
    f = urllib2.urlopen(url, timeout=10)
    content_structure = BeautifulSoup(f.read(), 'html.parser')

    siteid = '5029'
    if hasattr(content_structure, 'article mar-t-20'):
        article_module = content_structure.find('section', attrs={'class': 'article mar-t-20'})
        article_module_title = article_module.findAll('h3')
        for h in article_module_title:
            if hasattr(h.find('a'), 'href'):
                article_title = h.find('a').text
        article_module_time = content_structure.find('span', attrs={'class': 'mar-l-10'}).text
        # print article_module_time[0:10]
        article_module_editor_part = content_structure.find('p', attrs={'class': 'cont-msg mar-t-10'})
        editor = ''
        for s in article_module_editor_part.findAll('span'):
            editor += s.text
        article_module_editor = editor.replace(article_module_time, '')
        # print article_module_editor
        if hasattr(content_structure, 'cont article-detail-inner'):
            article_module_detail = content_structure.find('div', attrs={'class': 'cont article-detail-inner'})
            content_module = str(article_module_detail).replace('\n', '').replace('\r', '').replace('<p>', '').replace('</p>','').strip()

    article_id = url.split('/')[5].split('.')[0]
    article_user = article_module_editor
    article_tags = ''
    article_time = article_module_time[0:10]
    article_url = url
    article_cover = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,content_module,article_tags,article_time,siteid,article_url,article_cover)
    return article

# print crawlforcaixin('"http://english.caixin.com/2016-05-24/100947088.html"')
# if __name__=="__main__":
#     fileName = sys.argv[1]
#     errorfile = sys.argv[2]
#     input_file_list = str(sys.argv[3]).split(',')
#     for file_dir in input_file_list:
#         data = loadData(file_dir)
#         completefile = str(sys.argv[1])+'.complete'
#
#         # data = loadData('/home/elvis/caixin_error2')
#         # data = ['http://www.sc.xinhuanet.com/content/2016-09/20/c_1119591228.htm']
#         for url in data:
#             try:
#                 record = stardailycrawlforcontent(url)
#                 print record
#                 writeDataSet(fileName, record)
#             except:
#                 # print url
#                 url = url+'\n'
#                 writeDataSet(errorfile, url)
#     os.rename(fileName, completefile)

if __name__ == "__main__":
    # fileName = sys.argv[1]
    # errorfile = sys.argv[2]
    # input_file_list = str(sys.argv[3]).split(',')
    init_page = 'http://app.stardaily.com.cn/?app=rss'
    current_url = stardaily_crawl_for_url_list(init_page)
    current_id = current_url.split('/')[5].split('.')[0]
    current_date = '{}{}'.format(current_url.split('/')[3], current_url.split('/')[4])
    # print current_id, current_date
    content_url = 'http://www.stardaily.com.cn/{}/{}.shtml'

    date_list = datemaker('2016-01-01')
    past_id = int(current_id)
    for crawl_date in date_list:
        new_type_date = '{}/{}{}'.format(crawl_date.split('-')[0], crawl_date.split('-')[1], crawl_date.split('-')[2])
        global error_id
        error_id = 0
        while(error_id < 300):
            print past_id
            print error_id
            test_url = content_url.format(new_type_date, past_id)
            status_code = validate_content_url(test_url)
            if status_code == 200:
                try:
                    record = stardailycrawlforcontent(test_url)
                    print test_url
                    crawl_id = past_id
                    writeDataSet('/Users/elvis/5029', record)
                except:
                    pass
                past_id = past_id - 1

            if status_code == 404:
                error_id = error_id + 1
                past_id = past_id - 1
        past_id = crawl_id
    # for file_dir in input_file_list:
    #     data = loadData(file_dir)
    #     completefile = str(sys.argv[1])+'.complete'
    #
    #     # data = loadData('/home/elvis/caixin_error2')
    #     # data = ['http://www.sc.xinhuanet.com/content/2016-09/20/c_1119591228.htm']
    #     for url in data:
    #         try:
    #             record = stardailycrawlforcontent(url)
    #             print record
    #             writeDataSet(fileName, record)
    #         except:
    #             # print url
    #             url = url + '\n'
    #             writeDataSet(errorfile, url)
    # os.rename(fileName, completefile)
