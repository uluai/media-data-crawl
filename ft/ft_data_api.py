# -*- coding:utf-8 -*-
__author__ = 'elvis'
import pymongo
import urllib2
import traceback
import time
import json
import datetime


class FTDataApi(object):
    def __init__(self, host, port):
        self.client = pymongo.MongoClient(host=host, port=port)

    @staticmethod
    def get_url_info_by_urllib(init_url, code_type, headers):
        fails = 0
        page_content = ''
        while True:
            if fails >= 5:
                break
            try:
                request = urllib2.Request(init_url, headers=headers)
                f = urllib2.urlopen(request, timeout=10)
                if code_type == 'no':
                    page_content = f.read()
                else:
                    page_content = f.read().decode(code_type)
                break
            except urllib2.URLError, e:
                print 'error url: {}'.format(init_url)
                if hasattr(e, "code"):
                    print 'error code: {}'.format(e.code)
                if hasattr(e, "reason"):
                    print 'error reason: {}'.format(e.reason)
                else:
                    print 'other error reason:'
                    print traceback.format_exc()
                time.sleep(2)
                fails += 1
            finally:
                pass
        return page_content

    def article_api(self, input_date, header):
        article_api_url = 'http://www.ftchinese.com/index.php/jsapi/feeds/{}'.format(input_date)
        result = self.get_url_info_by_urllib(article_api_url, 'utf-8', header)
        article_list = json.loads(result, strict=False)
        if len(article_list) > 0:
            if type(article_list[0]) is list:
                new_list = []
                for item in article_list[0]:
                    item['_id'] = item['feedID']
                    new_list.append(item)
                try:
                    self.client.ft.ft_news.insert_many(new_list, ordered=False)
                except Exception, e:
                    print traceback.format_exc()
                    pass
                    # for item in article_list[0]:
                    #     try:
                    #         if self.client.ft.news.count({'_id': item.get('feedID')}) == 0:
                    #             self.client.ft.news.insert_many(item)
                    #     except Exception, e:
                    #         print traceback.format_exc()
                    #         pass

    def register_user_api(self, input_date, header):
        register_api_url = 'http://www.ftchinese.com/index.php/jsapi/users/{}'.format(input_date)
        result = self.get_url_info_by_urllib(register_api_url, 'utf-8', header)
        article_list = json.loads(result, strict=False)
        if len(article_list) > 0:
            if type(article_list[0]) is list:
                new_list = []
                for item in article_list[0]:
                    item['_id'] = item['userID']
                    new_list.append(item)
                try:
                    self.client.ft.ft_user.insert_many(new_list, ordered=False)
                except Exception, e:
                    print traceback.format_exc()
                    pass

    def comments_api(self, header):
        result = self.client.ft.ft_news.find()
        for item in result:
            feed_id = item['feedID']
            comment_api_url = 'http://www.ftchinese.com/index.php/jsapi/comments/{}'.format(feed_id)
            try:
                comments = json.loads(self.get_url_info_by_urllib(comment_api_url, 'utf-8', header))
            except:
                comments = []
                pass
            try:
                self.client.ft.ft_news.update({'feedID': feed_id}, {"$set": {'comments': comments}}, upsert=False)
            except Exception, e:
                print traceback.format_exc()
                pass


if __name__ == '__main__':
    mongo_host = '60.205.190.85'
    mongo_port = 10001
    ft = FTDataApi(mongo_host, mongo_port)
    BASIC_HEADER = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                  "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                    # 'Content-Type': 'application/json'
                    }
    current_date = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d')
    begin = datetime.date(2018, 1, 1)
    end = datetime.date(2018, 3, 7)
    for i in range((end - begin).days + 1):
        day = begin + datetime.timedelta(days=i)
        # print str(day)
        try:
            ft.article_api(str(day), BASIC_HEADER)
            ft.register_user_api(str(day), BASIC_HEADER)
        except Exception, e:
            print traceback.format_exc()
            print str(day)
            pass
    ft.comments_api(BASIC_HEADER)
