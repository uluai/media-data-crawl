__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import os
import datetime
import time
import hashlib
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            f = urllib2.urlopen(listpage,timeout=10)
            page_content = json.loads(f.read())
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content

def datemaker(input_date):
    date_list = []
    for i in range(0,10000):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

def encodeByMD5(src):
    m2 = hashlib.md5()
    m2.update(src)
    return m2.hexdigest()

# def sortedDictValues(adict):
#     keys = adict.keys()
#     keys.sort()
#     return map(adict.get, keys)

# encode content list sign key
def encodeContentListSign(date):
    sign = 'published={}&size=1000cdb52cf337a1cb7931f3a4ec96a3a270'.format(date)
    md5_sign = encodeByMD5(sign)
    return md5_sign

def encodeContentSign(id):
    sign = 'contentid={}cdb52cf337a1cb7931f3a4ec96a3a270'.format(id)
    md5_sign = encodeByMD5(sign)
    return md5_sign

def getContentPage(content,siteid,url):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url\001catagory
    article_id = content['contentid']
    article_title = content['title']
    article_tags = content['tags']
    article_time = content['published']
    article_url = content['url']
    content_page_url = url.format(encodeContentSign(article_id),article_id)
    content_page = getPage(content_page_url)
    article_content= content_page['data']['content'].replace('\n','').replace('\r','').replace('<p>','').replace('</p>','').replace('&ldquo','').replace('&rdquo','').strip()
    article_author = content_page['data']['author']
    article_cover = content_page['data']['thumb']
    article_catagory = content['catid']

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover,article_catagory)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover,article_catagory):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover,article_catagory)
    return article


if __name__=="__main__":

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_date = sys.argv[3]
    completefile = str(sys.argv[1])+'.complete'

    init_content_list_page = 'http://api.xinhua08.com/?app=system&controller=content&action=ls&key=84c666707e12a7e0e0b1363b6f316700&sign={}&published={}&size=1000'
    init_content_page = 'http://api.xinhua08.com/?app=article&controller=article&action=get&key=84c666707e12a7e0e0b1363b6f316700&sign={}&contentid={}'
    siteid = '5028'
    datalist = datemaker(input_date)

    for date in datalist:
        time.sleep(3)
        initpage = init_content_list_page.format(encodeContentListSign(date),date)
        # print initpage
        pagelist = getPage(initpage)
        # print len(pagelist['data']['items'])
        for item in pagelist['data']:
            try:
                article = getContentPage(item,siteid,init_content_page)
                # print article
                # time.sleep(10)
                writeDataSet(fileName, article)
            except:
                error_record = str(item) + '\n'
                writeDataSet(errorfile, error_record)
                pass
    os.rename(fileName, completefile)

