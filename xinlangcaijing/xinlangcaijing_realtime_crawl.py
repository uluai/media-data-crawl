# -*- coding:utf-8 -*-
__author__ = 'elvis'

import urllib2
import time
from bs4 import BeautifulSoup
import json
import sys
import os
import traceback


def write_data_set(file_name, data):
    fr = open(file_name, 'a')
    fr.write(data)


def get_url_info_by_urllib(init_url, code_type):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            # print f.read().decode('gbk')
            page_content = f.read().decode(code_type)
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp


def parse_js(expr):
    import ast
    m = ast.parse(expr)
    a = m.body[0]

    def parse(node):
        if isinstance(node, ast.Expr):
            return parse(node.value)
        elif isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.Str):
            return node.s
        elif isinstance(node, ast.Name):
            return node.id
        elif isinstance(node, ast.Dict):
            return dict(zip(map(parse, node.keys), map(parse, node.values)))
        elif isinstance(node, ast.List):
            return map(parse, node.elts)
        else:
            raise NotImplementedError(node.__class__)

    return parse(a)

def get_page_content(page_content):
    record = []
    content_structure = page_content.replace('var jsonData = ', '').encode('utf-8')
    content = parse_js(content_structure)
    if content is not None:
        for item in content['list']:
            try:
                url = item['url']
                id = url.split('doc-')[1].replace('.shtml', '')
                tags = ['财经']
                tags = json.dumps(tags)
                publish_time = item['time']
                mobile_page_url = '{}?from=wap'.format(url.replace('.com', ''))
                article_content = BeautifulSoup(get_url_info_by_urllib(mobile_page_url, 'utf-8'), 'html.parser')

                article_info = article_content.find('article', attrs={'class': 'art_box'})

                if article_info is not None:
                    editor = u'新浪财经'.encode('utf-8')
                    if article_info.find('cite', attrs={'class': 'art_cite'}) is not None:
                        editor = article_info.find('cite', attrs={'class': 'art_cite'}).text.encode('utf-8')
                    elif article_info.find('h2', attrs={'class': 'weibo_user'}) is not None:
                        editor = article_info.find('h2', attrs={'class': 'weibo_user'}).text.encode('utf-8')
                    title = article_info.find('h1', attrs={'class': 'art_tit_h1'}).text.encode('utf-8')
                    temp = ''
                    for article_p in article_info.find_all('p', attrs={'class': 'art_p'}):
                        detail = article_p.text.encode('utf-8').strip().replace('\n', '').replace('\r', '')
                        temp += detail
                    site_id = '7001'

                    cover = []
                    content_type = '0'
                    try:
                        article_img_list = article_info.find_all('img', attrs={'class': 'art_img_mini_img j_'
                                                                                        'fullppt_cover'})
                        if len(article_img_list) > 0:
                            for article_img in article_img_list:
                                cover.append('https:{}'.format(article_img.get('data-src')))
                            if 0 < len(cover) < 3:
                                content_type = '1'
                            if len(cover) > 2:
                                content_type = '2'
                    except:
                        pass
                    cover = json.dumps(cover)
                    source = 'sina'
                    category = 'caijing'
                    record.append(get_article_record(id, title, editor, temp, tags, publish_time, site_id, url, cover,
                                                     content_type, source, category))
            except Exception, e:
                print traceback.format_exc()
                pass
    return record


def get_article_record(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type, source, category):

    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor,
                                                                                              content, tags,
                                                                                              publishtime, siteid, url,
                                                                                              cover, content_type,
                                                                                              source, category)
    return article


if __name__ == '__main__':
    fileName = '{}_xinlangcaijing'.format(sys.argv[1])
    errorfileName = sys.argv[2]
    completefile = str(fileName) + '.complete'
    # xinlangcaijing_url_list = ['http://roll.news.sina.com.cn/s/channel.php?ch=01#col=97&spec=&type=&ch=01&k'
    #                            '=&offset_page=0&offset_num=0&num=100&asc=&page=1']
    xinlangcaijing_url_list = ['http://roll.news.sina.com.cn/interface/rollnews_ch_out_interface.php?col=97&spec=&type=&ch=01&k=&offset_page=0&offset_num=0&num=60&asc=&page=1']
    # for i in range(2, 65):
    #     xinlangcaijing_url_list.append('http://roll.news.sina.com.cn/interface/rollnews_ch_out_interface.php?col=97&'
    #                                    'spec=&type=&ch=01&k=&offset_page=0&offset_num=0&num=60&asc=&page={}'.format(i))
    for xinlangcaijing_url in xinlangcaijing_url_list:
        # print xinlangcaijing_url
        result_list = get_page_content(get_url_info_by_urllib(xinlangcaijing_url, 'gbk'))
        if len(result_list) > 0:
            for each_result in result_list:
                # print each_result
                write_data_set(fileName, each_result)
            # time.sleep(5)
    os.rename(fileName, completefile)

