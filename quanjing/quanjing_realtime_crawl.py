# -*- coding:utf-8 -*-
__author__ = 'elvis'

import urllib2
import time
from bs4 import BeautifulSoup
import json
import sys
import os


def write_data_set(file_name, data):
    fr = open(file_name, 'a')
    fr.write(data)


def get_url_info_by_urllib(init_url):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read().decode('gb2312')
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y%m%d %H:%M")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp


def get_page_content(page_content):
    record = []
    cover = []
    cover = json.dumps(cover)
    content_structure = BeautifulSoup(page_content, 'html.parser')
    content = content_structure.find('div', attrs={'class': 'manlist3'})
    if content is not None:
        for item in content.find_all('li'):
            try:
                url = item.h1.a.get('href')
                id = url.split('_')[1].replace('.htm', '')
                title = ''
                tags = []
                if ']' in item.h1.a.text.strip().encode('utf-8'):
                    title = item.h1.a.text.strip().encode('utf-8').split(']')[1]
                    tags.append(item.h1.a.text.strip().encode('utf-8').split(']')[0].replace('[', ''))
                else:
                    title = item.h1.a.text.strip().encode('utf-8')
                tags = json.dumps(tags)
                publish_time = get_timestamp('2017{}'.format(item.div.text.replace(u'月', '').replace(u'日', '')))

                article_content = BeautifulSoup(get_url_info_by_urllib(url), 'html.parser')
                editor_info = article_content.find('span', attrs={'class': 'left'})
                editor = editor_info.find_all('i')[2].text.encode('utf-8')
                temp = ''
                for article_p in article_content.find('div', attrs={'class': 'article_content2'}).find_all('p'):
                    detail = article_p.text.strip().encode('utf-8').replace('\n', '').replace('\r', '')
                    temp += detail
                site_id = '7001'

                content_type = '0'
                source = 'p5w'
                category = 'kuaixun'
                record.append(get_article_record(id, title, editor, temp, tags, publish_time, site_id, url, cover,
                                                 content_type, source, category))
            except:
                # print 'error'
                pass
    return record


def get_article_record(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type, source,
                       category):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor,
                                                                                              content, tags,
                                                                                              publishtime, siteid, url,
                                                                                              cover, content_type,
                                                                                              source, category)
    return article


if __name__ == '__main__':
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1]) + '.complete'
    quanjing_url_list = ['http://www.p5w.net/kuaixun/tj/index.htm']
    # for i in range(1, 20):
    #     quanjing_url_list.append('http://www.p5w.net/kuaixun/tj/index_{}.htm'.format(i))
    for quanjing_url in quanjing_url_list:
        # print quanjing_url
        result_list = get_page_content(get_url_info_by_urllib(quanjing_url))
        if len(result_list) > 0:
            for each_result in result_list:
                # print write_data
                write_data_set(fileName, each_result)
            time.sleep(5)
    os.rename(fileName, completefile)
