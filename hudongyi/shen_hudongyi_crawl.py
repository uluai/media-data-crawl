# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import re
import hashlib
import urllib
import httplib2
import time
import os
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 Firefox/35.0", "Cookie": "BDUSS=AAAAAAAAAAAAAAAAAAAAAAAA"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    list_page_content = BeautifulSoup(list_page.read(), 'html.parser', from_encoding="utf8")
    result_list = list_page_content.findAll('p', attrs={'style': 'float:left; width:700px;'})
    for item in result_list:
        # print item
        title = item.find('a', attrs={'class': 'ask'}).text.replace('\t', '').replace('\r\n', '').replace(' ', '')
        url = 'http://irm.cninfo.com.cn/ircs/interaction/{}'.format(
            item.find('a', attrs={'class': 'ask'}).get('href').replace('\t', '').replace('\r\n', ''))
        news_info = item.find('span', attrs={'class': 'time'}).text.replace('\t', '').replace('\r\n', '').split('（')[1].split('）')[0]
        source_time = news_info.split(',')[0]
        status = news_info.split(',')[1]
        generate_result(title, url, source_time, status)
        # print item
    # pass


def generate_result(title, url, source_time, status):
    myMd5 = hashlib.md5()
    myMd5.update(url)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}'.format(id, title, url, source_time, status)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://irm.cninfo.com.cn/ircs/interaction/lastQuestionforSzseSsgs.do?condition.type=2&condition.stockcode=002638&condition.stocktype=S'
    # init_page = 'http://news.baidu.com/ns?word=勤上光电&pn={}&cl=2&ct=0&tn=news&rn=20&ie=utf-8&bt=0&et=0'
    get_news_list(init_page)