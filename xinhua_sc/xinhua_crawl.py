__author__ = 'elvis'
# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
import urllib2
import sys
import time
import os

reload(sys)
sys.setdefaultencoding('utf8')

def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    print len(loaddata)
    return loaddata

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)
    # fr.close()

def crawlforxinhua(url):
    # url = 'http://weekly.caixin.com/2016-01-01/100895160.html'
    result = ''
    if 'content' in url:
        result = xinhuacrawlforcontent(url)

    # elif 'photos.caixin.com' in url:
    #     print 2

    # elif 'video.caixin.com' in url:
    #     result = caixincrawlforvideo(url)
    #
    # elif 'english.caixin.com' in url:
    #     result = caixincrawlforenglish(url)
    #
    # else:
    #     result = caixincrawlfornormal(url)
    return result


def xinhuacrawlforcontent(url):
    f = urllib2.urlopen(url,timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    siteid = '5008'
    article_id = url.split('/')[6].split('.')[0]
    article_title = content.head.title.get_text().strip()
    article_user = content.body.find('div',attrs={'class':'sus'}).get_text().strip()
    content_module = content.body.find('div',attrs={'id':'content'}).get_text().replace('\n','').replace('\r','').strip()
    if 'function' in content_module:
        article_content = ''
    else:
        article_content = content_module
    article_tags = ''
    article_time = content.body.find('div',attrs={'class':'tm'}).get_text().strip()[0:10]
    article_url = url
    article_cover = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

# print crawlforcaixin('"http://english.caixin.com/2016-05-24/100947088.html"')
if __name__=="__main__":
    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_file_list = str(sys.argv[3]).split(',')
    for file_dir in input_file_list:
        data = loadData(file_dir)
        completefile = str(sys.argv[1])+'.complete'

        # data = loadData('/home/elvis/caixin_error2')
        # data = ['http://www.sc.xinhuanet.com/content/2016-09/20/c_1119591228.htm']
        for url in data:
            try:
                record = crawlforxinhua(url)
                time.sleep(3)
                # print record
                writeDataSet(fileName, record)
            except:
                # print url
                url = url+'\n'
                writeDataSet(errorfile, url)
    os.rename(fileName, completefile)
