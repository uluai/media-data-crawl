# -*- coding: utf-8 -*-

__author__ = 'elvis'

import sys
import urllib2
import httplib2
import json
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')

def pv_api(url):
    h = httplib2.Http(".cache")
    resp, content = h.request(url, "GET")
    data = json.loads(content)
    return data

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

if __name__ == '__main__':
    # word_id =1
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    start_id = sys.argv[3]
    end_id = sys.argv[4]
    for word_id in range(int(start_id),int(end_id)):
        word_tags =''
        redirectpage = ''
        try:
            init_url = 'http://baike.baidu.com/view/{}.htm'.format(word_id)
            url = 'http://baike.baidu.com/api/lemmapv?id={}&r=0.29981099674121436'
            f = urllib2.urlopen(init_url,timeout=10)
            print f.geturl()
            redirectpage = f.geturl()
            pagesource = urllib2.urlopen(redirectpage,timeout=10)
            content = BeautifulSoup(pagesource.read(), 'html.parser')
            word =  content.find(attrs={'class':'lemmaWgt-lemmaTitle-title'}).h1.get_text()
            if content.findAll(attrs={'class':'taglist'}) != None:
                for item in content.find_all(attrs={'class':'taglist'}):
                    # print item.string.strip()
                    word_tags = word_tags + item.get_text().strip() + '\002'
                # print word_tags

            # print str(content)
            lemmaid =  str(content).split('newLemmaIdEnc')[1].split('"')[1]
            url = 'http://baike.baidu.com/api/lemmapv?id={}&r=0.29981099674121436'.format(lemmaid)
            word_pv = pv_api(url)['pv']

            # id/001词条/001标签/001频次
            word_record = '{}\001{}\001{}\001{}\n'.format(word_id,word,word_tags,word_pv)
            # print word_record
            writeDataSet(fileName, word_record)
        except:
            error_record = str(word_id) +'\001'+init_url+'\001'+ redirectpage + '\n'
            writeDataSet(errorfileName, error_record)

