# -*- coding: utf-8 -*-

__author__ = 'elvis'

import sys
import urllib2
import httplib2
import json
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')

def pv_api(url):
    h = httplib2.Http(".cache")
    resp, content = h.request(url, "GET")
    data = json.loads(content)
    return data

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    return loaddata

if __name__ == '__main__':
    # word_id =1
    fileName = sys.argv[1]
    outputfile = sys.argv[2]
    errorfileName = sys.argv[3]
    error_list = loadData(fileName)
    for error_record in error_list:
        word_tags =''
        redirectpage = ''
        # if 'error' not in error_record:
        # url = 'http://baike.baidu.com/api/lemmapv?id={}&r=0.29981099674121436'
        error_info = error_record.split('\001')
        try:
            pagesource = urllib2.urlopen(error_info[1],timeout=10)
            print error_info[0]
            # print pagesource.getcode()
            redirectpage = pagesource.geturl()
            if 'http://baike.baidu.com/error.html' not in redirectpage:
                pagesource = urllib2.urlopen(redirectpage,timeout=10)
                content = BeautifulSoup(pagesource.read(), 'html.parser')
                if 'newLemmaIdEnc' in str(content):
                    if content.findAll(attrs={'class':'taglist'}) != None:
                        word =  content.find(attrs={'class':'lemmaWgt-lemmaTitle-title'}).h1.get_text()
                        for item in content.find_all(attrs={'class':'taglist'}):
                            # print item.string.strip()
                            word_tags = word_tags + item.get_text().strip() + '\002'
                        # print word_tags

                        # print str(content)
                        lemmaid = str(content).split('newLemmaIdEnc')[1].split('"')[1]
                        url = 'http://baike.baidu.com/api/lemmapv?id={}&r=0.29981099674121436'.format(lemmaid)
                        word_pv = pv_api(url)['pv']

                        # id/001词条/001标签/001频次
                        word_record = '{}\001{}\001{}\001{}\n'.format(error_info[0],word,word_tags,word_pv)
                        # print word_record
                        # print word_record
                        writeDataSet(outputfile, word_record)
        except:
            error_record = str(error_info[0]) +'\001'+error_info[1]+'\001'+ redirectpage + '\n'
            # print error_record
            writeDataSet(errorfileName, error_record)