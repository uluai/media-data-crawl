# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import re
import hashlib
import urllib
import httplib2
import time
import os
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    # headers = {"User-Agent": "Mozilla/5.0 Firefox/35.0", "Cookie": "BDUSS=AAAAAAAAAAAAAAAAAAAAAAAA"}
    # request = urllib2.Request(page, headers=headers)
    # list_page = urllib2.urlopen(request)
    # print list_page.read()
    h = httplib2.Http(".cache")
    resp, content = h.request(page, "GET")
    list_page_content = BeautifulSoup(content, 'html.parser')
    result_list = list_page_content.find('ul', attrs={'class':'news-list'}).findAll('li')
    for item in result_list:
        # print item
        title = item.h3.text.replace('\n', '')
        url = item.h3.a.get('href')
        news_info = item.find('div', attrs={'class': 's-p'})
        source = news_info.a.text
        source_url = news_info.a.get('href')
        source_time = news_info.get('t')
        summary = item.find('p', attrs={'class': 'txt-info'}).text
        page_view = ''
        if news_info.find('span', attrs={'class': 's1'})!= None:
            page_view = news_info.find('span', attrs={'class': 's1'}).text
        generate_result(title, url, source, source_time, summary, page_view, source_url)


def generate_result(title, url, source, source_time, summary, page_view, source_url):
    myMd5 = hashlib.md5()
    myMd5.update(title)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}'.format(
        id, title, url, source, source_time, summary, page_view, source_url)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://weixin.sogou.com/weixin?query=勤上光电&from=tool&ft=&tsn=1&et=&interation=null&type=2&wxid=&page={}&ie=utf8'
    for p in range(1, 10):
        search_page = init_page.format(p)
        get_news_list(search_page)