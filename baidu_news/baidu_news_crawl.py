# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import re
import hashlib
import urllib
import httplib2
import time
import os
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 Firefox/35.0", "Cookie": "BDUSS=AAAAAAAAAAAAAAAAAAAAAAAA"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    list_page_content = BeautifulSoup(list_page.read(), 'html.parser')
    result_list = list_page_content.findAll('div', attrs={'class': 'result'})
    for item in result_list:
        title = item.h3.text
        url = item.h3.a.get('href')
        news_info = item.div.p.string
        print news_info
        new_author = re.sub(u'[^\u4e00-\u9fa5|0-9|:|\w]', '\t', str(news_info).decode('utf-8'))
        source = new_author.split('\t\t')[0]
        source_time = new_author.split('\t\t')[1]
            # news_info.replace(re.compile('[^\u4e00-\u9fa5]'),' ')
        summary = item.text
        generate_result(title, url, source, source_time, summary)
        # print item
    # pass


def generate_result(title, url, source, source_time, summary):
    myMd5 = hashlib.md5()
    myMd5.update(title)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}\001{}'.format(id, title, url, source, source_time, summary)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://news.baidu.com/ns?word=%C7%DA%C9%CF%B9%E2%B5%E7&sr=0&cl=2&rn=20&pn={}&tn=news&ct=0&clk=sortbytime'
    # init_page = 'http://news.baidu.com/ns?word=勤上光电&pn={}&cl=2&ct=0&tn=news&rn=20&ie=utf-8&bt=0&et=0'
    get_news_list(init_page)