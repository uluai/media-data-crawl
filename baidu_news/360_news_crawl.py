# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import re
import hashlib
import urllib
import httplib2
import time
import os
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 Firefox/35.0", "Cookie": "BDUSS=AAAAAAAAAAAAAAAAAAAAAAAA"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    list_page_content = BeautifulSoup(list_page.read(), 'html.parser')
    result_list = list_page_content.findAll('li', attrs={'class': 'res-list','data-tag': True})
    for item in result_list:
        print item
        title = item.h3.text
        url = item.h3.a.get('href')
        news_info = item.find('p', attrs={'class':'newsinfo'})
        source = news_info.find('span', attrs={'class': 'sitename'}).text
        source_time = news_info.find('span', attrs={'class': 'posttime'}).get('data-pdate')
        summary = item.find('p', attrs={'class':'content'}).text
        generate_result(title, url, source, source_time, summary)


def generate_result(title, url, source, source_time, summary):
    myMd5 = hashlib.md5()
    myMd5.update(title)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}\001{}'.format(id, title, url, source, source_time, summary)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    init_page = 'http://news.so.com/ns?j=0&rank=pdate&src=sort_time&scq=&q=勤上光电&pn=1'
    # init_page = 'http://news.baidu.com/ns?word=勤上光电&pn={}&cl=2&ct=0&tn=news&rn=20&ie=utf-8&bt=0&et=0'
    get_news_list(init_page)