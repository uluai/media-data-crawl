# -*- coding:utf-8 -*-
__author__ = 'elvis'

import sys
import urllib2
import re
import hashlib
import urllib
import httplib2
import time
import os
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(filename, data):
    fr = open(filename, 'a')
    fr.write(data)


def get_news_list(page):
    headers = {"User-Agent": "Mozilla/5.0 Firefox/35.0", "Cookie": "BDUSS=AAAAAAAAAAAAAAAAAAAAAAAA"}
    request = urllib2.Request(page, headers=headers)
    list_page = urllib2.urlopen(request)
    # print list_page.read()
    list_page_content = BeautifulSoup(list_page.read(), 'html.parser', from_encoding="gb18030")
    result_list = list_page_content.findAll('div', attrs={'class': 'vrwrap'})
    for item in result_list:
        # print item
        if item.div.find('h3', attrs={'class': 'vrTitle'}):
            title = item.div.find('h3', attrs={'class': 'vrTitle'}).text.replace('\n', '')
            url = item.div.h3.a.get('href')
            news_info = item.find('div', attrs={'class':'news-info'})
            news_from = news_info.find('p', attrs={'class': 'news-from'}).text
            # print news_from
            # new_author = news_from.split(' ')[0]
            new_author = re.sub(u'[^\u4e00-\u9fa5|0-9|:|.|\w|-]', '\t', str(news_from).decode('utf-8'))
            source = new_author.split('\t')[0]
            source_time = new_author.split('\t')[1]
            summary = news_info.find('p', attrs={'class': 'news-txt'}).span.text
            generate_result(title, url, source, source_time, summary)


def generate_result(title, url, source, source_time, summary):
    myMd5 = hashlib.md5()
    myMd5.update(title)
    id = myMd5.hexdigest()
    new_result = '{}\001{}\001{}\001{}\001{}\001{}'.format(id, title, url, source, source_time, summary)
    print new_result

def write_data_to_kafka():
    pass

def write_data_to_file():
    pass

def def_news_detail(page):
    pass


if __name__ == "__main__":
    '&sort=1&page=2'
    init_page = 'http://news.sogou.com/news?mode=1&query=%C7%DA%C9%CF&sort=1&page={}&_asf=news.sogou.com'
    for p in range (1,10):
        search_page = init_page.format(p)
    # init_page = 'http://news.baidu.com/ns?word=勤上光电&pn={}&cl=2&ct=0&tn=news&rn=20&ie=utf-8&bt=0&et=0'
        get_news_list(search_page)