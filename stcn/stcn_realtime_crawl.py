__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib2
import time
from bs4 import BeautifulSoup
import os


reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def read_xml(node,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url

    # article_title = ''
    # article_tags = ''
    # article_url = ''
    article_cover = ''
    # article_time = ''
    # article_id = ''
    article_author = ''
    # article_content = ''

    article_title = node.find('title').text
    article_url = node.find('link').text
    article_tags = node.find('category').text
    # article_cover = node.find('image').text
    article_id = article_url.split('.com/')[1].split('/')[2].split('.')[0]
    article_time = article_url.split('.com/')[1].split('/')[0] +'-'+ article_url.split('.com/')[1].split('/')[1][0:2]+'-'+article_url.split('.com/')[1].split('/')[1][2:4]
    article_content = str(node.find('description').text).replace('\n','').replace('\r','').strip()

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article


# def getContentPage(url):
#     f = urllib.urlopen(url)
#     content = BeautifulSoup(f.read(), 'html.parser')
#     article_content = content.body.find('div',attrs={'class':'t-neirong t_box_public'}).get_text().replace('\n','').replace('\r','').strip()
#     return article_content


def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

if __name__=="__main__":
    initpage = 'http://app.stcn.com/?app=rss&controller=index&action=feed&catid=340'
    siteid = '5003'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'

    f = urllib2.urlopen(initpage,timeout=10)
    content = BeautifulSoup(f.read(),'xml')
    for node in content.findAll('item'):
        try:
            article = read_xml(node,siteid)
            # print article
            writeDataSet(fileName, article)
            time.sleep(1)
        except:
            error_record = str(node)+'\n'
            writeDataSet(errorfileName, error_record)
            pass
    os.rename(fileName, completefile)

