__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib2
import json
import time
from bs4 import BeautifulSoup
import os


reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName, 'a')
    fr.write(data)

def read_xml(node, siteid):
    temp = ''
    article_url = node.find('info_url').text
    article_title = node.find('title').text
    article_tags = node.find('cid_str').text
    article_cover = ''
    article_cover = node.find('img_url').text
    article_time = time_transfer(node.find('push_time').text)
    article_id = node.find('id').text
    article_author = ''
    article_content = BeautifulSoup(node.find('content').text, 'html.parser')
    for item in article_content.find_all('p'):
        detail = item.text.replace('\t', '').replace('\n', '')
        temp += detail
    content_type = '0'
    if article_cover != '':
        content_type = '1'
    article = getArticleRecord(article_id, article_title, article_author, temp, article_tags,
                               article_time, siteid, article_url, article_cover, content_type)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover, content_type):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover,content_type)
    return article

def time_maker(timestamp):
    time_tag = int(time.time()) - 86400
    if float(timestamp) >= time_tag:
        return True
    return False

def time_transfer(timestamp):
    timeArray = time.strptime(timestamp, u"%Y-%m-%d %H:%M")
    time_tag = int(time.mktime(timeArray))
    return time_tag

def stop_crawl_time_maker(timestamp, time_tag):
    timeArray = time.strptime(time_tag, u"%Y-%m-%d")
    stop_time_tag = int(time.mktime(timeArray)) - 86400
    if float(timestamp) >= stop_time_tag:
        return True
    return False

if __name__ == "__main__":
    app_init_url = 'http://wmapp.chengdu.cn/api/newspull/xml/token/xpshx368'
    app_siteid = '5051'
    fileName = ''
    errorfileName = ''
    time_tag = ''
    if len(sys.argv) <= 3:
        fileName = sys.argv[1]
        errorfileName = sys.argv[2]
        completefile = str(sys.argv[1]) + '.complete'
        try:
            f = urllib2.urlopen(app_init_url)
            content = BeautifulSoup(f.read(), 'xml')
        except:
            print "Error, could not load page {}.".format(app_init_url)
        for node in content.channel.findAll('item'):
            try:
                if time_maker(time_transfer(node.find('push_time').text)):
                    article = read_xml(node, app_siteid)
                    if article != '':
                        # print article
                        writeDataSet(fileName, article)
                    # time.sleep(1)
            except:
                error_record = str(node) + '\n'
                writeDataSet(errorfileName, error_record)
                pass
        if os.path.exists(fileName):
            os.rename(fileName, completefile)

    if len(sys.argv) == 4:
        fileName = sys.argv[1]
        errorfileName = sys.argv[2]
        time_tag = sys.argv[3]
        completefile = str(sys.argv[1]) + '.complete'
        try:
            f = urllib2.urlopen(app_init_url)
            content = BeautifulSoup(f.read(), 'xml')
        except:
            print "Error, could not load page {}.".format(app_init_url)
        for node in content.channel.findAll('item'):
            try:
                if stop_crawl_time_maker(time_transfer(node.find('push_time').text), time_tag):
                    article = read_xml(node, app_siteid)
                    if article != '':
                        # print article
                        writeDataSet(fileName, article)
                        # time.sleep(1)
            except:
                error_record = str(node) + '\n'
                writeDataSet(errorfileName, error_record)
                pass
        if os.path.exists(fileName):
            os.rename(fileName, completefile)


