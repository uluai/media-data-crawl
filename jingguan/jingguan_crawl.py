__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib
import time
from bs4 import BeautifulSoup
import os


reload(sys)
sys.setdefaultencoding('utf8')

def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    print len(loaddata)
    return loaddata

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def read_xml(title,link,image,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url

    # article_title = ''
    article_tags = ''
    # article_url = ''
    # article_cover = ''
    # article_time = ''
    # article_id = ''
    article_author = ''
    # article_content = ''

    article_title = title
    article_url = link.replace('ttp:','http:')
    # article_tags = node.find('category').text
    article_cover = image.replace('ttp:','http:')
    article_id = article_url.split('cn/')[1].split('/')[2].split('.')[0]
    article_time = article_url.split('cn/')[1].split('/')[0] +'-'+ article_url.split('cn/')[1].split('/')[1][0:2]+'-'+article_url.split('cn/')[1].split('/')[1][2:4]
    mobile_article_url = article_url.replace('www','m')
    article_content = getContentPage(mobile_article_url)

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article


def getContentPage(url):
    f = urllib.urlopen(url)
    content = BeautifulSoup(f.read(), 'html.parser')
    article_content = content.body.find('div',attrs={'class':'t-neirong t_box_public'}).get_text().replace('\n','').replace('\r','').strip()
    return article_content


def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

if __name__=="__main__":
    # initpage = 'http://www.eeo.com.cn/rss.xml'
    siteid = '5019'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'

    history = loadData('/home/elvis/test.txt')

    # f = urllib.urlopen(initpage)
    # content = BeautifulSoup(f.read(),'xml')
    # for node in content.findAll('item'):
    for item in history:
        each_line = item.split(',h')
        try:
            article = read_xml(each_line[0],each_line[1],each_line[2],siteid)
            print article
            writeDataSet(fileName, article)
            time.sleep(1)
        except:
            error_record = str(item)+'\n'
            writeDataSet(errorfileName, error_record)
            pass
    os.rename(fileName, completefile)

