__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import os
import datetime

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    f = urllib2.urlopen(listpage,timeout=10)
    page_content = json.loads(f.read())
    return page_content

def getContentPage(content,siteid):


    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = content['post_id']
    article_title = content['title']
    article_tags = content['tags']
    article_time = time.strftime("%Y-%m-%d", time.localtime(int(content['time'])))
    article_url = content['post_url']
    article_content = content['main'].strip().replace('\n','').replace('\r','')
    article_author = content['authors']
    article_cover = content['image']

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

def datemaker(input_date):
    date_list = []
    for i in range(0,100):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

if __name__=="__main__":

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_date = sys.argv[3]
    completefile = str(sys.argv[1])+'.complete'
    lasttime = ''

    flag = 0
    status = 0
    initpage = 'http://www.tmtpost.com/api/YouluCommend/article_list?date={}&size=100'
    siteid = '5022'
    date_list = datemaker(input_date)


    for crawl_date in date_list:
        crawl_date = crawl_date.replace('-','')
        listpage = initpage.format(crawl_date)
        # print listpage
        pagelist = getPage(listpage)
        # print len(pagelist['data']['items'])
        for item in pagelist:
            try:
                article = getContentPage(item,siteid)
                # print article
                # time.sleep(10)
                writeDataSet(fileName, article)
            except:
                error_record = str(item) + '\n'
                writeDataSet(errorfile, error_record)
                pass
    os.rename(fileName, completefile)
