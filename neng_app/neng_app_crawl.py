__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib
import json

reload(sys)
sys.setdefaultencoding('utf8')

channels = {'22','33','21','19','12','17','14','13','43'}
# channels = {'37'}

# http://api.nengapp.com/v1/info/ios/news/tags
# http://api.nengapp.com/v1/info/ios/news/special?tagId=22&limit=100
# http://api.nengapp.com/v1/info/ios/news/717071

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    f = urllib.urlopen(listpage)
    page_content = json.loads(f.read())
    return page_content

def getContentPage(content,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = content['aId']
    article_title = content['title']
    article_tags = " ".join(str(x) for x in content['tags'])
    article_time = content['date'][0:10]
    article_url = content['rawUrl']
    pagecontent = getPage('http://api.nengapp.com/v1/info/ios/news/{}'.format(article_id))
    article_content = pagecontent['data']['content'].strip().replace('\n','').replace('\r','')
    article_author = pagecontent.get('data').get('author','')
    try:
        article_cover = content['media'][0]['mUrl']
    except:
        article_cover = ''

    # article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,'5002',article_url,article_cover)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

if __name__=="__main__":
    initpage = 'http://api.nengapp.com/v1/info/ios/news/special?tagId={}&limit=100'
    siteid = '5014'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]

    for tid in channels:
        listpage = initpage.format(tid)
        flag = 0
        while(flag == 0):
            pagelist = getPage(listpage)
            listpage = pagelist.get('data','').get('nextUrl','')
            print listpage
            for item in pagelist['data']['lists']:
                try:
                    article = getContentPage(item,siteid)
                    # print article
                    time.sleep(3)
                    writeDataSet(fileName, article)
                except:
                    error_record = str(item)+'\n'
                    writeDataSet(errorfileName, error_record)
                    pass
            if listpage == '':
                flag = 1





