__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import datetime
import os

reload(sys)
sys.setdefaultencoding('utf8')

channels = {'22','25','33','21','19','12','17','14','13','43','24','37'}

# http://api.nengapp.com/v1/info/ios/news/tags
# http://api.nengapp.com/v1/info/ios/news/special?tagId=22&limit=100
# http://api.nengapp.com/v1/info/ios/news/717071

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    f = urllib2.urlopen(listpage,timeout=10)
    page_content = json.loads(f.read())
    return page_content

def getContentPage(content,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = content['aId']
    article_title = content['title']
    article_tags = " ".join(str(x) for x in content['tags'])
    article_time = content['date'][0:10]
    article_url = content['rawUrl']
    pagecontent = getPage('http://api.nengapp.com/v1/info/ios/news/{}'.format(article_id))
    article_content = pagecontent['data']['content'].strip().replace('\n','').replace('\r','')
    article_author = pagecontent.get('data').get('author','')
    try:
        article_cover = content['media'][0]['mUrl']
    except:
        article_cover = ''

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

def datemaker(input_date):
    date_list = []
    for i in range(0,100):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

if __name__=="__main__":
    initpage = 'http://api.nengapp.com/v1/info/ios/news/special?tagId={}&limit=100'
    siteid = '5014'
    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_date = sys.argv[3]
    completefile = str(sys.argv[1])+'.complete'

    date_list = datemaker(input_date)


    for tid in channels:
        status = 0
        listpage = initpage.format(tid)
        while(True):
            pagelist = getPage(listpage)
            print listpage
            for item in pagelist['data']['lists']:
                publishtime = item['date'][0:10]
                if publishtime in date_list:
                    try:
                        article = getContentPage(item,siteid)
                        listpage = pagelist['data']['nextUrl']
                        time.sleep(3)
                        writeDataSet(fileName, article)
                    except:
                        error_record = str(item)+'\n'
                        writeDataSet(errorfile, error_record)
                        pass
                if publishtime not in date_list:
                    status = 1
                    break
            if status == 1:
                break
    os.rename(fileName, completefile)
