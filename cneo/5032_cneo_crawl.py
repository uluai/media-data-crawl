__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import os
import datetime
import time
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            f = urllib2.urlopen(listpage,timeout=10)
            page_content = f.read()
            break
        except:
            time.sleep(3)
            fails +=1
    return page_content


def getContentPage(content,siteid,aid,content_page):

    detail = BeautifulSoup(content, 'html.parser')


    article_id = aid
    article_title = detail.find('h1',attrs={'class':'ph'}).text
    article_tags = ''
    article_time = detail.find('div',attrs={'class':'h hm'}).p.text.replace('\n','')[:10].split('-')
    tup = (int(article_time[0]), int(article_time[1]), int(article_time[2]), 0, 0, 0, 0, 0, 0)
    t = time.mktime(tup)
    article_time = str(time.strftime("%Y-%m-%d", time.localtime(t)))
    article_url = content_page
    article_content= detail.find('td',attrs={'id':'article_content'}).text.replace('\n','').replace('\r','').replace('<p>','').replace('</p>','').strip()
    article_author = ''
    article_cover = ''

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article


if __name__=="__main__":

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'


    flag = 0
    status = 0
    init_content_page = 'http://www.cneo.com.cn/article-{}-1.html'
    siteid = '5032'
    # date_list = datemaker(input_date)

    status = 0
    for aid in range(1,21150):
        print aid
        # time.sleep(5)
        content_page = init_content_page.format(aid)

        # print content_page
        content_info = getPage(content_page)

        try:
            article = getContentPage(content_info,siteid,aid,content_page)
            # print article
            writeDataSet(fileName, article)
        except:
            error_record = 'error_record, ' + str(content_page) + '\n'
            writeDataSet(errorfile, error_record)
            pass
        aid += 1
    os.rename(fileName, completefile)

