# -*- coding:utf-8 -*-
__author__ = 'elvis'

import urllib2
import time
from bs4 import BeautifulSoup
import json
import sys
import os
import traceback


def write_data_set(file_name, data):
    fr = open(file_name, 'a')
    fr.write(data)


def get_url_info_by_urllib(init_url, code_type):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read().decode(code_type)
            break
        except:
            time.sleep(2)
            fails += 1
    return page_content


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp


def get_page_content(page_content):
    record = []
    if page_content is not None:
        for item in json.loads(page_content):
            try:
                tags = []
                if u'财经' in item['tag']:
                    for tag in item['tag']:
                        tags.append(tag.encode('utf-8'))
                    tags = json.dumps(tags)
                    url = item['url']
                    title = item['title'].encode('utf-8')
                    id = item['id']
                    publish_time = get_timestamp(item['date'])
                    editor = item['source'].encode('utf-8')
                    site_id = '7001'
                    cover = []
                    for pic in item['pic']:
                        cover.append(pic)
                    content_type = '0'
                    if 0 < len(cover) < 3:
                        content_type = '1'
                    if len(cover) > 2:
                        content_type = '2'
                    cover = json.dumps(cover)
                    source = 'huanqiu'
                    category = 'news'
                    article_content = item['content'].encode('utf-8').strip().replace('\n', '').replace('\r', '')
                    record.append(
                        get_article_record(id, title, editor, article_content, tags, publish_time, site_id, url, cover,
                                           content_type, source, category))
            except Exception, e:
                print traceback.format_exc()
                pass
    return record


def get_article_record(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type, source,
                       category):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor,
                                                                                              content, tags,
                                                                                              publishtime, siteid, url,
                                                                                              cover, content_type,
                                                                                              source, category)
    return article


if __name__ == '__main__':
    fileName = '{}_huanqiunews'.format(sys.argv[1])
    errorfileName = sys.argv[2]
    completefile = str(fileName) + '.complete'

    huanqiu_news_url_list = ['http://m.huanqiu.com/apps/uluai/news.php']
    # for i in range(2, 301):
    #     yicainews_url_list.append('http://m.yicai.com/ajax/jhcommonlist/news/10/{}'.format(i))
    for huanqiu_news_url_list in huanqiu_news_url_list:
        result_list = get_page_content(get_url_info_by_urllib(huanqiu_news_url_list, 'utf-8'))
        if len(result_list) > 0:
            for each_result in result_list:
                print each_result
                write_data_set(fileName, each_result)
                #         # time.sleep(5)
    os.rename(fileName, completefile)
