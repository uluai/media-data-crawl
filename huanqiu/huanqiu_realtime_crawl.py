__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib2
import time
import os
from bs4 import BeautifulSoup


reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def read_xml(node,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url

    # article_title = ''
    article_tags = ''
    # article_url = ''
    # article_time = ''
    # article_id = ''
    article_author = ''
    # article_content = ''

    article_title = node.find('title').text
    article_url = node.find('link').text
    article_id = article_url.split('r/')[1]
    article_time = str(node.find('pubDate').text)[0:10]

    article_content, article_cover = getContentPage(article_url)
    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article


def getContentPage(url):
    f = urllib2.urlopen(url,timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    content_body = content.body.find('div',attrs={'id':'text'})
    article_content = ''
    article_cover = ''
    if content_body.find('img'):
        article_cover = content_body.find('img').attrs['src']
    for paragraph in content_body.findAll('p'):
        article_content = article_content + paragraph.get_text().replace('\n','').replace('\r','').strip()
    return article_content, article_cover


def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

if __name__=="__main__":
    initpage = 'http://m.huanqiu.com/rss'
    siteid = '5011'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'

    f = urllib2.urlopen(initpage,timeout=10)
    content = BeautifulSoup(f.read(),'xml')
    for node in content.findAll('item'):
        try:
            article = read_xml(node,siteid)
            # print article
            writeDataSet(fileName,article)
            time.sleep(2)
        except:
            error_record = str(node)+'\n'
            # print error_record
            writeDataSet(errorfileName, error_record)
            pass
    os.rename(fileName, completefile)

