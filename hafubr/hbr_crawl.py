__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    f = urllib.urlopen(listpage)
    page_content = json.loads(f.read())
    return page_content

def getContentPage(content,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = content['id']
    article_title = content['title']
    # article_time = content['article_time']
    article_tags = ''
    article_time = time.strftime("%Y-%m-%d", time.localtime(content['publishTime']))
    article_url = content['articleUrl']
    pagecontent = getPage('{}&appVer=1&platform=youlu&udid=null&bundleId=org.hbrchina.iphonenews&magazines=hbr&devicemachine=null&network=wifi'.format(article_url))
    article_content = pagecontent['data']['content'].strip().replace('\n','').replace('\r','')
    article_author = pagecontent['data']['author']
    if content['thumbnails_url'] != None:
        article_cover = content['path']+content['thumbnails_url'][0]
    else:
        article_cover = ''

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

if __name__=="__main__":
    id = '1423818779'
    # id = ''
    flag = 0
    initpage = 'http://api.caijingmobile.com/api/2.0/cms_articles.php?columnId=1019&action=articlelist&lasttime={}' \
               '&appVer=1&platform=youlu&udid=null&bundleId=org.hbrchina.iphonenews&magazines=hbr&devicemachine=null&network=wifi&pagesize=200'
    # initpage = 'http://api.caijingmobile.com/api/2.0/cms_articles.php?columnId=1019&action=articlelist&lasttime=1423818779&appVer=1&platform=youlu&udid=null&bundleId=org.hbrchina.iphonenews&magazines=hbr&devicemachine=null&network=wifi&pagesize=200'
    siteid = '5013'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]


    while(flag < 9000):
        listpage = initpage.format(id)
        # print listpage
        pagelist = getPage(listpage)
        for item in pagelist['data']['articles']:
            try:
                article = getContentPage(item,siteid)
                id = item['publishTime']
                print id
                # print article
                time.sleep(15)
                writeDataSet(fileName, article)
            except:
                error_record = str(item)+'\n'
                writeDataSet(errorfileName, error_record)
                pass
            flag = flag + 1
            print flag
