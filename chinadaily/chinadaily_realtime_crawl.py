__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import os
import datetime
import time

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            f = urllib2.urlopen(listpage,timeout=10)
            page_content = json.loads(f.read())
            break
        except:
            time.sleep(3)
            fails +=1
    return page_content


def getContentPage(content,siteid):


    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = content['articleId']
    article_title = content['title']
    article_tags = content['feature']
    article_time = time.strftime("%Y-%m-%d", time.localtime(content['publishTime']))
    article_url = content['shareUrl']
    # pagecontent = content['content']
    article_content = str(content['content']).replace('\n','').replace('\r','').replace('<p>','').replace('</p>','').strip()
    article_author = content['author']
    if content['thumbnails'] > 0:
        article_cover = str(content['shareUrl']).replace('article.html','')+'thumbnail2_{}@2x.jpg'.format(content['articleId'])
    else:
        article_cover = ''

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

def datemaker(input_date):
    date_list = []
    for i in range(0,10000):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

if __name__=="__main__":

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_date = sys.argv[3]
    completefile = str(sys.argv[1])+'.complete'

    channels = []

    flag = 0
    status = 0
    channel_page = 'http://iosnews.chinadaily.com.cn/newsdata/news/source_v4.json'
    init_content_page = 'http://iosnews.chinadaily.com.cn/newsdata/news/columns.shtml?page={}&columnId={}'
    siteid = '5031'
    date_list = datemaker(input_date)

    channel_info = getPage(channel_page)

    for item in channel_info['columns']:
        if channel_info['columns'][item]['type']  == 0:
            channels.append(item)
    # print channels
    for channel_id in channels:
        page_num = 0
        status = 0
        while(True):
            time.sleep(5)
            content_page = init_content_page.format(page_num,channel_id)
            page_num = page_num+1
            print content_page
            content_info = getPage(content_page)
            # print content_info
            if content_info !='':
                crawled_page = 'crawled_page, ' + content_page + '\n'
                writeDataSet(errorfile, crawled_page)
                for content_id in content_info['articles']:
                    publishtime = time.strftime("%Y-%m-%d", time.localtime(content_info['articles'][content_id]['publishTime']))
                    if publishtime in date_list:
                        try:
                            article = getContentPage(content_info['articles'][content_id],siteid)
                            # print article
                            writeDataSet(fileName, article)
                        except:
                            error_record = 'error_record, ' + str(content_info['articles']) + '\n'
                            writeDataSet(errorfile, error_record)
                            pass
                    # flag = flag + 1
                    if publishtime not in date_list:
                        status = status + 1
                    if status >= 3:
                        break
            if content_info =='':
                err = 'error_page, '+content_page + '\n'
                writeDataSet(errorfile, err)
            if status >= 3:
                break

    os.rename(fileName, completefile)

