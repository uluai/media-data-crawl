1. 安装 beautifulsoup 包
2. 安装c语言环境，gcc
3. 各个媒体的爬虫可配置输入输出地址和一些常规参数（参数之间空格练习）。

## 百度百科爬虫运行参数配置：

```
python baike_crawl.py 词条记录保存地址 错误词条id保存地址 开始词条id 结束词条id
```

## 百度百科错误url爬虫运行参数配置：

```
python baike_error_parser.py 待爬错误url文件地址 正确词条记录保存地址 错误词条记录保存地址
```

## 四川新华网爬虫运行参数配置：

```
python xinhua_crawl.py 已爬文章内容保存地址 错误url保存地址 待爬url文件地址
```

##　财新网爬虫运行参数配置：

```
python caixin_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址 爬取文章时间戳（例如：2016-09-07）
```

##　财经杂志app爬虫运行参数配置：

```
python caijing_app_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址 爬取文章时间戳（例如：2016-09-07）
```

##　哈评（网站+app）爬虫运行参数配置：

```
python hbr_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址 爬取文章时间戳（例如：2016-09-07）
```

##　能见app爬虫运行参数配置：

```
python neng_app_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址 爬取文章时间戳（例如：2016-09-07）
```

##　经观网爬虫运行参数配置：

```
python jingguan_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址
```

##　环球网爬虫运行参数配置：

```
python huanqiu_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址
```

##　TMT app爬虫运行参数配置：

```
python tmt_realtime_crawl.py 已爬文章内容保存地址 错误url保存地址 爬取文章时间戳（例如：2016-09-07）
```

