# -*- coding:utf-8 -*-
__author__ = 'elvis'

import urllib2
import time
from bs4 import BeautifulSoup
import json
import sys
import os
import traceback


def write_data_set(file_name, data):
    fr = open(file_name, 'a')
    fr.write(data)


def get_url_info_by_urllib(init_url, code_type):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
                       "Host": "m.yicai.com",
                       "Referer": "http://m.yicai.com/news/"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read().decode(code_type)
            break
        except:
            time.sleep(2)
            fails += 1
    return page_content


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y-%m-%d %H:%M")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp

def get_page_content(page_content):
    record = []
    if page_content is not None:
        for item in json.loads(page_content):
            try:
                url = 'http:{}'.format(item['url'])
                title = item['NewsTitle'].encode('utf-8')
                id = item['NewsID']
                tags = []
                tags.append(item['ChannelName'].encode('utf-8'))
                tags = json.dumps(tags)
                publish_time = get_timestamp(item['pubDate'])
                editor = item['NewsAuthor'].encode('utf-8')
                site_id = '7001'
                cover = []
                if item['thumbPic'] != '':
                    cover.append(item['thumbPic'])
                content_type = '0'
                if 0 < len(cover) < 3:
                    content_type = '1'
                if len(cover) > 2:
                    content_type = '2'
                cover = json.dumps(cover)
                source = 'yicai'
                category = 'news'
                article_content = BeautifulSoup(get_url_info_by_urllib(url, 'utf-8'), 'html.parser')
                article_info = article_content.find('div', attrs={'class': 'txt'})
                temp = ''
                if article_info is not None:
                    for article_p in article_info.find_all('p'):
                        detail = article_p.text.encode('utf-8').strip().replace('\n', '').replace('\r', '')
                        temp += detail
                record.append(get_article_record(id, title, editor, temp, tags, publish_time, site_id, url, cover,
                                                 content_type, source, category))
            except Exception, e:
                print traceback.format_exc()
                pass
    return record


def get_article_record(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type, source, category):

    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor,
                                                                                              content, tags,
                                                                                              publishtime, siteid, url,
                                                                                              cover, content_type,
                                                                                              source, category)
    return article


if __name__ == '__main__':
    fileName = '{}_yicainews'.format(sys.argv[1])
    errorfileName = sys.argv[2]
    completefile = str(fileName) + '.complete'

    yicainews_url_list = ['http://m.yicai.com/ajax/jhcommonlist/news/10/1']
    # for i in range(2, 301):
    #     yicainews_url_list.append('http://m.yicai.com/ajax/jhcommonlist/news/10/{}'.format(i))
    for yicainews_url in yicainews_url_list:
        print yicainews_url
        result_list = get_page_content(get_url_info_by_urllib(yicainews_url, 'gbk'))
        if len(result_list) > 0:
            for each_result in result_list:
                # print each_result
                write_data_set(fileName, each_result)
            # time.sleep(5)
    os.rename(fileName, completefile)

