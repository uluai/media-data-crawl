__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib2
import time
import os
import json
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')


def write_data_set(fileName, data):
    fr = open(fileName, 'a')
    fr.write(data)


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp


def read_xml(node, siteid):
    covers = []
    tags = []
    content_type = '0'
    article_tag = node.find('tag').text.encode('utf-8').strip()
    article_author = node.find('author').text.encode('utf-8').strip()
    article_content = node.find('description').text.encode('utf-8').strip().replace('\n', '').replace('\r', '')
    article_cover = node.find('url').text.encode('utf-8').strip()
    article_title = node.find('title').text.encode('utf-8').strip()
    article_url = node.find('link').text.encode('utf-8').strip()
    article_id = node.find('id').text.encode('utf-8').strip()
    article_time = get_timestamp(node.find('pubDate').text.encode('utf-8').strip())
    article_source = node.find('source').text.encode('utf-8').strip()

    if article_cover is not None:
        covers.append(article_cover)
        content_type = '1'
    if article_tag is not None:
        tags.append(article_tag)
    covers = json.dumps(covers)
    tags = json.dumps(tags)

    article = getArticleRecord(article_id, article_title, article_author, article_content, tags, article_time,
                               siteid, article_url, covers, content_type, article_source)
    return article


def get_page_content(url):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read()
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content


def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover, content_type, source):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor, content,
                                                                                        tags, pulishtime, siteid, url,
                                                                                        cover, content_type, source)
    return article


if __name__ == "__main__":
    initpage = 'http://www.nbd.com.cn/rss/platform/youlu.xml'
    siteid = '5049'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1]) + '.complete'
    content = BeautifulSoup(get_page_content(initpage), 'xml')
    for node in content.findAll('item'):
        try:
            article = read_xml(node, siteid)
            # print article
            write_data_set(fileName, article)
            # time.sleep(2)
        except:
            error_record = str(node) + '\n'
            # print error_record
            write_data_set(errorfileName, error_record)
            pass
    os.rename(fileName, completefile)
