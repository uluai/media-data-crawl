# -*- coding:utf-8 -*-
__author__ = 'elvis'

import urllib2
import time
from bs4 import BeautifulSoup
import json
import sys
import os
import traceback


def write_data_set(file_name, data):
    fr = open(file_name, 'a')
    fr.write(data)


def get_url_info_by_urllib(init_url, code_type='utf-8'):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read().decode(code_type)
            break
        except:
            time.sleep(2)
            fails += 1
    return page_content


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y-%m%d")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp


def get_page_content(page_content):
    record = []
    if page_content is not None:
        page_structure = BeautifulSoup(page_content, 'html.parser')
        for item in page_structure.find_all('ul', attrs={'class': 'plist'}):
            try:
                url = item.find('div', attrs={'class': 'txt f-r'}).h3.a.get('href')
                title = item.find('div', attrs={'class': 'txt f-r'}).h3.a.text.encode('utf-8')
                id = url.split('/')[5].replace('.shtml', '')
                tags = []
                tags = json.dumps(tags)
                publish_time = get_timestamp('{}-{}'.format(url.split('/')[3], url.split('/')[4]))
                editor = ''
                site_id = '5052'
                cover = []
                pic = item.find('div', attrs={'class': 'pic f-l'}).find('img').get('src')
                cover.append(pic)
                content_type = '0'
                if 0 < len(cover) < 3:
                    content_type = '1'
                if len(cover) > 2:
                    content_type = '2'
                cover = json.dumps(cover)
                source = 'chengdu'
                category = 'video'
                article_content = item.text.encode('utf-8').replace('\n', '').replace('\r', '')
                record.append(
                    get_article_record(id, title, editor, article_content, tags, publish_time, site_id, url, cover,
                                       content_type, source, category))
            except Exception, e:

                print traceback.format_exc()
                pass
    return record


def get_article_record(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type, source,
                       category):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor,
                                                                                              content, tags,
                                                                                              publishtime, siteid, url,
                                                                                              cover, content_type,
                                                                                              source, category)
    return article


if __name__ == '__main__':
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(fileName) + '.complete'

    chengdu_vedio_url_list = ['http://video.chengdu.cn/aksp/msdj/', 'http://video.chengdu.cn/aksp/akxw/',
                              'http://video.chengdu.cn/aksp/akrw/', 'http://video.chengdu.cn/aksp/akcd/',
                              'http://video.chengdu.cn/aksp/aktx/', 'http://video.chengdu.cn/aksp/akdrd/',
                              'http://video.chengdu.cn/aksp/akzb/', 'http://video.chengdu.cn/aksp/akft/']
    for chengdu_url in chengdu_vedio_url_list:
        # print chengdu_url
        result_list = get_page_content(get_url_info_by_urllib(chengdu_url))
        if len(result_list) > 0:
            for each_result in result_list:
                print each_result
                write_data_set(fileName, each_result)
                # time.sleep(5)
    os.rename(fileName, completefile)
