# -*- coding:utf-8 -*-
__author__ = 'elvis'


from bs4 import BeautifulSoup
import urllib2
import time
import sys
import os
import datetime
import re
import json
import traceback

def save_data(result):
    for item in result:
        with open('/Users/elvis/Desktop/result', 'a') as nf:
            nf.write(item+'\n')


def write_data_set(file_name, data):
    fr = open(file_name,'a')
    fr.write(data)


def get_url_info_by_urllib(init_url):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read()
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content


def get_url_list_from_title_page(list_url):
    url_list = []
    list_structure = BeautifulSoup(get_url_info_by_urllib(list_url), 'html.parser')
    nlist = list_structure.find('ul', attrs={'class': 'nlist'})
    article_list = nlist.find_all('li')
    for item in article_list:
        url_list.append(item.a.get('href'))
    return url_list


def get_url_list_from_photo_page(list_url):
    url_list = []
    list_structure = BeautifulSoup(get_url_info_by_urllib(list_url), 'html.parser')
    section_left = list_structure.find('div', attrs={'class': 'section_left'})
    article_list = section_left.find_all('ul', attrs={'class': 'plist'})
    for item in article_list:
        url_list.append(item.find('h3').a.get('href'))
    return url_list


def get_page_content(news_url):
    page_structure = BeautifulSoup(get_url_info_by_urllib(news_url), 'html.parser')
    if len(page_structure.find_all('section', attrs={'class': 'column picture-wrap'})) == 0:
        temp = ''
        cover = []
        news_detail = page_structure.find('div', attrs={'class': 'newsdetail_content font'})
        id = news_url.split('/')[5].replace('.shtml', '')
        title = news_detail.find('h1', attrs={'id': 'art_title'}).text
        publish_time = get_timestamp(news_detail.find('h2', attrs={'class': 'font_gray'}).text)
        try:
            editor = news_detail.find('h2', attrs={'class': 'font_gray'}).a.text.replace('\t', '').replace('\n', '')
        except:
            editor = ''
            pass
        for item in news_detail.find_all('p'):
            detail = item.text
            temp += detail
        tags = 'news'
        site_id = '5048'
        content_type = '0'
        for item in news_detail.find('div', attrs={'class': 'article-content fontSizeSmall BSHARE_POP'}).find_all('img'):
            if 'chengdu.cn' in item.get('src'):
                if 'nopic.gif' not in item.get('src'):
                    cover.append(item.get('src'))
        if 0 < len(cover) < 3:
            content_type = '1'
        if len(cover) > 2:
            content_type = '2'
        cover = json.dumps(cover)
        record = get_article_record(id, title.encode('utf-8'), editor.encode('utf-8'), temp.encode('utf-8'), tags,
                                    publish_time, site_id, news_url, cover, content_type)
        return record
    if len(page_structure.find_all('section', attrs={'class': 'column picture-wrap'})) > 0:
        cover = []
        news_detail = page_structure.find('div', attrs={'class': 'picture-main'})
        id = news_url.split('/')[5].replace('.shtml', '')
        title = news_detail.find('header', attrs={'class': 'picture-header'}).h1.text
        publish_time = get_timestamp(news_detail.find('span', attrs={'class': 'post-time'}).text)
        try:
            editor = news_detail.find('span', attrs={'class': 'source'}).a.text.replace('\t', '').replace('\n', '')
        except:
            editor = ''
            pass
        content = news_detail.find('p', attrs={'class': 'summary'}).text
        tags = 'news'
        site_id = '5048'
        content_type = '0'
        for item in news_detail.find('div', attrs={'class': 'gallery-photo'}).find_all('img'):
            if 'chengdu.cn' in item.get('src'):
                if 'nopic.gif' not in item.get('src'):
                    cover.append(item.get('src'))
        if 0 < len(cover) < 3:
            content_type = '1'
        if len(cover) > 2:
            content_type = '2'
        cover = json.dumps(cover)
        record = get_article_record(id, title.encode('utf-8'), editor.encode('utf-8'), content.encode('utf-8'), tags,
                                    publish_time, site_id, news_url, cover, content_type)
        return record


def get_article_record(id, title, editor, content, tags, publish_time, site_id, href, cover, content_type):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor, content, tags,
                                                                            publish_time, site_id, href, cover,
                                                                            content_type)
    return article


def get_timestamp(str):
    pattern = re.compile(r'\d{4}-\d{2}-\d{2}\s+([01][0-9]|2[0-3]):[0-5][0-9]')
    match = pattern.search(str)
    if match:
        timeArray = time.strptime(match.group(), u"%Y-%m-%d %H:%M")
        timeStamp = int(time.mktime(timeArray))
        return timeStamp


def date_maker(input_date):
    date_list = []
    for i in range(0,10000):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list


if __name__ == '__main__':

    output_file = sys.argv[1]
    error_file = sys.argv[2]
    date = sys.argv[3]
    complete_file = str(output_file) + '.complete'

    init_channel = ['http://news.chengdu.cn/xwsy/xinwenzixun/{}.shtml',
                    'http://news.chengdu.cn/xwsy/quyuxinwen/{}.shtml',
                    'http://news.chengdu.cn/xwsy/benwangzuixin/{}.shtml',
                    'http://news.chengdu.cn/xwsy/guoneiguoji/{}.shtml',
                    'http://news.chengdu.cn/xwsy/tupianxinwen/{}.shtml']

    crawl_urls = set()

    for url in init_channel:
        stop_tag = 0
        for i in range(1, 200):
            news_urls = []
            start_page = url.format(i)
            if 'tupianxinwen' in start_page:
                news_urls = get_url_list_from_photo_page(start_page)
            elif 'tupianxinwen' not in start_page:
                news_urls = get_url_list_from_title_page(start_page)
            for each_news_url in news_urls:
                if each_news_url is not None:
                    judge_date = '{}-{}-{}'.format(each_news_url.split('/')[3], each_news_url.split('/')[4][0:2],
                                                       each_news_url.split('/')[4][2:4])
                    if judge_date in date_maker(date):
                        crawl_urls.add(each_news_url)
                    if judge_date not in date_maker(date):
                        stop_tag = 1
            if stop_tag == 1:
                break

    for p_url in crawl_urls:
        try:
            write_data = get_page_content(p_url)
            # print write_data
            # if len(cover_data) > 0:
            write_data_set(sys.argv[1], write_data)
        except Exception, e:
            # print traceback.format_exc()
            write_data_set(sys.argv[2], p_url)
            write_data_set(sys.argv[2], traceback.format_exc())
            pass
    os.rename(output_file, complete_file)