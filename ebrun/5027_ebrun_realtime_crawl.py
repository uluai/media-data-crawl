__author__ = 'elvis'
# -*- coding: utf-8 -*-

import sys
import urllib2
import time
from bs4 import BeautifulSoup
import json
import re
import os
import datetime


reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            f = urllib2.urlopen(listpage,timeout=10)
            decodecontent =  f.read().decode('gbk').encode('utf-8').replace('\t','')
            page_content = json.loads(decodecontent)
            break
        except:
            time.sleep(3)
            fails +=1
    return page_content

def getContentPage(content,siteid):

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = content['id']
    article_title = content['title']
    article_tags = content['tag']
    article_time = content['date']
    article_url = content['url'].replace('\\','')
    article_content = content['content'].strip().replace('\n','').replace('\r','')
    article_author = content['author']
    article_cover = content['pic'].replace('\\','')

    article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article


# def getContentPage(url):
#     f = urllib.urlopen(url)
#     content = BeautifulSoup(f.read(), 'html.parser')
#     article_content = content.body.find('div',attrs={'class':'t-neirong t_box_public'}).get_text().replace('\n','').replace('\r','').strip()
#     return article_content


def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover)
    return article

def datemaker(input_date):
    date_list = []
    for i in range(0,10000):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

if __name__=="__main__":
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    input_date = sys.argv[3]
    completefile = str(sys.argv[1])+'.complete'

    datalist = datemaker(input_date)

    for date in datalist:
        time.sleep(3)
        initpage = 'http://www.ebrun.com/saveas/2016/uluai.php?date={}'.format(date)
        # print initpage
        siteid = '5027'
        pagelist = getPage(initpage)
        # print len(pagelist['data']['items'])
        for item in pagelist:
            try:
                article = getContentPage(item,siteid)
                # print article
                # time.sleep(10)
                writeDataSet(fileName, article)
            except:
                error_record = str(item) + '\n'
                writeDataSet(errorfileName, error_record)
                pass
    os.rename(fileName, completefile)

