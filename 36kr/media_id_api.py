__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import httplib2
import sys
import time

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getListPage(listpage):
    resp, content = h.request(listpage, "GET")
    pagelist = json.loads(content)
    return pagelist

def getContentPage(contentpage,article_user):

    item_resp, item_content = h.request(contentpage, "GET")
    pagecontent = json.loads(item_content)

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_id = str(pagecontent['data']['id'])
    article_title = pagecontent['data']['title']
    article_content = pagecontent['data']['content'].replace('\n','')
    article_tags = pagecontent['data']['extraction_tags']
    article_time = str(pagecontent['data']['created_at'])[0:10]
    article_url = 'http://36kr.com/p/{}.html'.format(article_id)
    article_cover = pagecontent['data']['cover']

    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

if __name__=="__main__":
    id = ''
    flag = True
    initpage = 'http://36kr.com/api/info-flow/main_site/posts?b_id={}&per_page=100'
    secondpage = 'http://36kr.com/api/post/{}'
    siteid = '5006'
    fileName = '/home/elvis/36krs_new'
    h = httplib2.Http(".cache")

    while(flag == True):
        listpage = initpage.format(id)
        print listpage
        pagelist = getListPage(listpage)
        # print len(pagelist['data']['items'])
        for i in pagelist['data']['items']:
            article_user = i['user']['name']
            contentpage = secondpage.format(i['id'])
            try:
                article = getContentPage(contentpage,article_user)
                time.sleep(0.07)
                writeDataSet(fileName, article)
            except:
                pass
            id = i['id']
        if len(pagelist['data']['items'])<100:
            flag = False