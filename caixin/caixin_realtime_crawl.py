# -*- coding: utf-8 -*-

__author__ = 'elvis'

from bs4 import BeautifulSoup
import httplib2
import urllib2
import sys
import json
import time
import datetime
import traceback
import os

reload(sys)
sys.setdefaultencoding('utf8')


def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    # print len(loaddata)
    return loaddata


def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)


def realtime_crawl_list(init_url):
    fails = 0
    page_content = ''
    data = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read()
            break
        except:
            time.sleep(3)
            fails += 1
    if page_content != '':
        data = json.loads(page_content)
    return data


def analyse_crawl_list(content):
    data = json.loads(content)
    for item in data['datas']:
        link = item['link']


def crawlforcaixin(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc):
    # url = 'http://weekly.caixin.com/2016-01-01/100895160.html'
    result = ''

    if 'datanews.caixin.com' in a_url:
        result = caixin_crawl_for_datanews(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc)

    elif 'gbiz.caixin.com' in a_url or 'video.caixin.com' in a_url:
        result = caixin_crawl_for_video(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc)

    elif 'caixinglobal' in a_url:
        result = caixin_crawl_for_english(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc)

    else:
        result = caixin_crawl_for_normal(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc)
    return result


def caixin_crawl_for_normal(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc):
    f = urllib2.urlopen(a_url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    article_user = ''
    article_content = ''
    content_structure = content.find('div', attrs={'id': 'Main_Content_Val'})
    for p in content_structure.find_all('p'):
        article_content += p.text.strip().replace('\n', '').replace('\r', '').replace(' ', '')
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(a_id, a_title, article_user,
                                                                                  article_content, a_tag, a_time,
                                                                                  a_siteid, a_url, a_cover, a_desc)
    return article

def caixin_crawl_for_datanews(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc):
    f = urllib2.urlopen(a_url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    article_user = ''
    article_content = ''
    for p in content.find_all('p'):
        article_content += p.text.strip().replace('\n', '').replace('\r', '').replace(' ', '')
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(a_id, a_title, article_user,
                                                                                  article_content, a_tag, a_time,
                                                                                  a_siteid, a_url, a_cover, a_desc)
    return article

def caixin_crawl_for_english(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc):
    f = urllib2.urlopen(a_url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    article_user = ''
    article_content = ''
    content_structure = content.find('div', attrs={'class': 'cons-box'})
    for p in content_structure.find_all('p'):
        article_content += p.text.replace('\n', '').replace('\r', '').replace(' ', '')
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(a_id, a_title, article_user,
                                                                                  article_content, a_tag, a_time,
                                                                                  a_siteid, a_url, a_cover, a_desc)
    return article


def caixin_crawl_for_video(a_url, a_time, a_title, a_cover, a_id, a_siteid, a_tag, a_desc):
    f = urllib2.urlopen(a_url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    article_user = ''
    try:
        article_content = content.find('div', attrs={'class': 'subhead'}).text.strip()
    except:
        article_content = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(a_id, a_title, article_user,
                                                                                  article_content, a_tag, a_time,
                                                                                  a_siteid, a_url, a_cover, a_desc)
    return article

def datemaker(input_date):
    date_list = []
    for i in range(0,100):
        date_time = datetime.datetime.now() + datetime.timedelta(days=-i)
        date_day = str(date_time)[0:10]
        # print date_day
        if date_day == input_date:
            date_list.append(date_day)
            break
        date_list.append(date_day)
    return date_list

if __name__=="__main__":
    status = 0
    siteid = '5004'
    # current_time = time.strftime("%y-%m-%d", time.localtime())
    # yes_time = datetime.datetime.now() + datetime.timedelta(days=-1)
    # yesterday_time = str(yes_time)[0:10]

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    input_date = sys.argv[3]
    completefile = str(sys.argv[1])+'.complete'
    # fileName = '/home/elvis/caixin_realtime'
    # errorfile = '/home/elvis/caixin_realtime_error'
    for i in range(1, 300):
        url = 'http://m.search.caixin.com/search/latest/mobjson.jsp?page={}'.format(i)
        aticle_list = realtime_crawl_list(url)
        date_list = datemaker(input_date)
        if aticle_list != '':
            for item in aticle_list['datas']:
                if 'http://m' not in item['link']:
                    item_time = str(item['time'])[0:10]
                    if item_time in date_list:
                        item_id = item['nid']
                        item_url = item['link']
                        item_title = item['desc']
                        item_cover = item['pict']
                        item_tag = item['chdesc']
                        item_desc = item['info']
                        try:
                            record = crawlforcaixin(item_url, item_time, item_title, item_cover, item_id, siteid, item_tag, item_desc)
                            # print record
                            writeDataSet(fileName, record)
                        except Exception, e:
                            # print traceback.format_exc()
                            # print item_url
                            url = item_url+'\n'
                            writeDataSet(errorfile, url)
                    if item_time not in date_list:
                        status = 1
                        break
                    if status == 1:
                        break
            if status == 1:
                break
    os.rename(fileName, completefile)
