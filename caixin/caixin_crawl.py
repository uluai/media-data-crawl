__author__ = 'elvis'
# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
import urllib
import sys

reload(sys)
sys.setdefaultencoding('utf8')

def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    print len(loaddata)
    return loaddata

def loadErrorData(fileName):
    fr = open(fileName)
    data = fr.readline()
    url_list = data.split(';')
    return url_list

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)
    # fr.close()

def crawlforcaixin(url):
    # url = 'http://weekly.caixin.com/2016-01-01/100895160.html'
    result = ''
    if 'www.caixin.com' in url:
        result = caixincrawlforwww(url)

    # elif 'photos.caixin.com' in url:
    #     print 2

    elif 'video.caixin.com' in url:
        result = caixincrawlforvideo(url)

    elif 'english.caixin.com' in url:
        result = caixincrawlforenglish(url)

    else:
        result = caixincrawlfornormal(url)
    return result


def caixincrawlfornormal(url):
    search_url = url.replace('://','://m.').replace('caixin.com/','caixin.com/m/').replace('"','')
    f = urllib.urlopen(search_url)
    content = BeautifulSoup(f.read(), 'html.parser')

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    siteid = '5004'
    article_id = url.split('/')[4].replace('.html','')
    article_title = content.body.find(attrs={'class': 'news-title'}).get_text()
    article_user = ''
    # article_user = content.body.find('div',attrs={'class':'author'}).get_text().strip()
    # print article_user
    article_content = content.body.find(attrs={'class':'news-con'}).get_text().strip()
    article_tags = ''
    article_time = url.split('/')[3]
    article_url = url
    article_cover = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def caixincrawlforenglish(url):
    search_url = url.replace('://','://m.').replace('caixin.com/','caixin.com/m/').replace('"','')
    f = urllib.urlopen(search_url)
    content = BeautifulSoup(f.read(), 'html.parser')

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    siteid = '5004'
    article_id = url.split('/')[4].replace('.html','')
    article_title = content.body.find('h1').get_text()
    article_user = ''
    # article_user = content.body.find('div',attrs={'class':'author'}).get_text().strip()
    # print article_user
    article_content = content.body.find(attrs={'class':'en-cons'}).get_text().strip()
    article_tags = ''
    article_time = url.split('/')[3]
    article_url = url
    article_cover = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def caixincrawlforvideo(url):
    search_url = url.replace('://','://m.').replace('caixin.com/','caixin.com/m/').replace('"','')
    f = urllib.urlopen(search_url)
    content = BeautifulSoup(f.read(), 'html.parser')

    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    siteid = '5004'
    article_id = url.split('/')[4].replace('.html','')
    article_title = content.body.find(attrs={'class': 'news-title'}).get_text()
    article_user = ''
    # article_user = content.body.find('div',attrs={'class':'author'}).get_text().strip()
    # print article_user
    article_content = content.body.find(attrs={'class':'news-lead'}).get_text().strip()
    article_tags = ''
    article_time = url.split('/')[3]
    article_url = url
    article_cover = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

def caixincrawlforwww(url):
    search_url = url.replace('://www.','://m.').replace('caixin.com/','caixin.com/m/').replace('"','')
    f = urllib.urlopen(search_url)
    content = BeautifulSoup(f.read(), 'html.parser')
    # 文章id\001标题\001作者\001内容\001标签\001时间(2016-08-23)\0015006\001文章的url\001图片的url
    siteid = '5004'
    article_id = url.split('/')[4].replace('.html','')
    article_title = content.body.find(attrs={'class': 'news-title'}).get_text()
    article_user = ''
    article_content = content.body.find(attrs={'class':'news-con'}).get_text().strip()
    article_tags = ''
    article_time = url.split('/')[3]
    article_url = url
    article_cover = ''
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(article_id,article_title,article_user,article_content,article_tags,article_time,siteid,article_url,article_cover)
    return article

# print crawlforcaixin('"http://english.caixin.com/2016-05-24/100947088.html"')
if __name__=="__main__":
    fileName = '/home/elvis/caixin_3'
    errorfile = '/home/elvis/caixin_error3'
    # data = loadData('/home/elvis/url_20160826-in2016.txt')
    data = loadData('/home/elvis/caixin_error2')
    for url in data:
        try:
            record = crawlforcaixin(url)
            writeDataSet(fileName, record)
        except:
            print url
            url = url+'\n'
            writeDataSet(errorfile, url)