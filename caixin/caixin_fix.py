# -*- coding: utf-8 -*-

__author__ = 'elvis'
# fix caixin article id

import sys
import re

reload(sys)
sys.setdefaultencoding('utf8')

def loadData(fileName):
    loaddata = []
    fr = open(fileName)
    for line in fr:
        loaddata.append(line.strip())
    print len(loaddata)
    return loaddata

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

if __name__ == '__main__':
    fileName = '/home/elvis/caixin'
    fixfile = '/home/elvis/caixin_fix'
    data = loadData(fileName)
    for item in data:
        fixed_item = re.compile('^\d{4}(\-)\d{2}(\-)\d{2}(\-)').sub('',item).replace('"','',1)+'\n'
        writeDataSet(fixfile,fixed_item)

