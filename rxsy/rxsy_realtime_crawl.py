__author__ = 'elvis'
# -*- coding: utf-8 -*-

import json
import sys
import time
import urllib2
import os
import datetime
import time
import traceback
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf8')

def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def getPage(listpage):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            f = urllib2.urlopen(listpage,timeout=10)
            page_content = f.read()
            break
        except:
            time.sleep(3)
            fails +=1
    return page_content

# def get_list_page(content):
#     detail = BeautifulSoup(content, 'html.parser')


def getListPage(content, siteid, category):

    result = []

    detail = BeautifulSoup(content, 'html.parser')

    list_section = detail.find('div', attrs={'id': 'content'})
    article_section = list_section.findAll('article')
    for item in article_section:
        try:
            article_summary = ''
            article_id = item.get('id')
            article_title = item.find('a').text
            article_url = item.find('a').get('href')
            if hasattr(item.find('div', attrs={'class': 'entry-summary'}), 'p'):
                if hasattr(item.find('div', attrs={'class': 'entry-summary'}).p, 'text'):
                    article_summary = item.find('div', attrs={'class': 'entry-summary'}).p.text
            article_tags = category
            article_content, article_time = getContentPage(article_url)
            article_author = ''
            article_cover = item.header.div.img.get('data-lazy-src')
            article = getArticleRecord(article_id,article_title,article_author,article_content,article_tags,article_time,siteid,article_url,article_cover,article_summary)
            result.append(article)
        except Exception, e:
            # exstr = traceback.format_exc()
            # print exstr
            pass
    return result

def getContentPage(article_url):
    content = BeautifulSoup(getPage(article_url), 'html.parser')
    article_content = str(content.find('div', attrs={'id': 'content'}).article.text).replace('\n', '').replace('\r', '')
    article_time = time_translator(content.find('time', attrs={'class': 'entry-date'}).get('datetime')[0:10])
    return article_content, article_time

def getArticleRecord(id, title, editor, content, tags, pulishtime, siteid, url, cover, article_summary):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,pulishtime,siteid,url,cover,article_summary)
    return article

def time_translator(t):
    timeArray = time.strptime(t, u"%Y-%m-%d")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp

if __name__=="__main__":

    fileName = sys.argv[1]
    errorfile = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'

    siteid = '5001'
    category = ['gallery', 'kids', 'howtos', 'creative', 'w']
    init_page = 'http://www.rxsy.net/category/{}/'
    for item in category:
        for num in range(1, 30):
            if num == 1:
                init_list_page = init_page.format(item)
                list_page_content = getPage(init_list_page)
            if num > 1:
                init_list_page = '{}page/{}/'.format(init_page.format(item), num)
                list_page_content = getPage(init_list_page)
            try:
                article_list = getListPage(list_page_content, siteid, item)
                for article in article_list:
                    # print article
                    writeDataSet(fileName, article)
            except:
                error_record = 'error_record, ' + str(list_page_content) + '\n'
                writeDataSet(errorfile, error_record)
                pass

    os.rename(fileName, completefile)

