# -*- coding:utf-8 -*-
__author__ = 'elvis'

import urllib2
import time
import MySQLdb
import datetime
from bs4 import BeautifulSoup
import json
import sys
import os
import traceback


def connect_database(connection):
    saved_goods_ids = []

    connection.execute("select * from goods_info")
    result = connection.fetchall()
    if len(result) > 0:
        for row in result:
            saved_goods_ids.append(row[0])
    return saved_goods_ids


def get_exist_comment_ids(connection):
    exist_comment_ids = []

    connection.execute("select * from goods_comment")
    result = connection.fetchall()
    if len(result) > 0:
        for row in result:
            exist_comment_ids.append(row[0])
    return exist_comment_ids


def insert_new_goods_info(connection, data):
    sql = "insert into goods_info (goods_id, goods_name, cate_id, cate_name, shop_price, market_price, goods_thumb, " \
          "stone_range, sales_count, stone, material, gender, gold_type, insert_time, update_time) values ({})".format(
        data)
    try:
        connection.execute(sql)
        conn.commit()
    except:
        pass


def insert_new_goods_comment(connection, comment_id, item, comment_user_id, comment_open_id,
                             comment_user_name, comment_content, comment_rank, comment_images,
                             comment_add_time):
    try:
        sql = "insert into goods_comment(id, goods_id, user_id, open_id, user_name, content, rank, images, add_time, " \
              "insert_time) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        param = (comment_id, item, comment_user_id, comment_open_id, comment_user_name, comment_content, comment_rank,
                 comment_images, comment_add_time, datetime.datetime.now())
        connection.execute(sql, param)
        conn.commit()
    except:
        pass

def update_goods_info(connection, id):
    sql = "update goods_info set update_time = %s where goods_id = %s"
    param = (datetime.datetime.now(), id)
    try:
        connection.execute(sql, param)
        conn.commit()
    except Exception, e:
        print traceback.format_exc()
        pass


def write_data_set(file_name, data):
    fr = open(file_name, 'a')
    fr.write(data)


def get_url_info_by_urllib(init_url, code_type):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
                       "Content-Type": "application/json"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            # print f.read().decode('gbk')
            page_content = f.read().decode(code_type)
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content


def get_timestamp(str):
    timeArray = time.strptime(str, u"%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp


def parse_js(expr):
    import ast
    m = ast.parse(expr)
    a = m.body[0]

    def parse(node):
        if isinstance(node, ast.Expr):
            return parse(node.value)
        elif isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.Str):
            return node.s
        elif isinstance(node, ast.Name):
            return node.id
        elif isinstance(node, ast.Dict):
            return dict(zip(map(parse, node.keys), map(parse, node.values)))
        elif isinstance(node, ast.List):
            return map(parse, node.elts)
        else:
            raise NotImplementedError(node.__class__)

    return parse(a)


def get_good_comment(id_list, connection):
    exist_comment_ids = get_exist_comment_ids(connection)
    for item in id_list:
        comment_url = 'http://www.lzzuan.com/api/cate.php?act=goods_comment&id={}'.format(item)
        good_comments_result = get_url_info_by_urllib(comment_url, 'utf-8')
        # goods_comments_list = parse_js(good_comments_result)
        goods_comments_list = json.loads(good_comments_result)
        if len(goods_comments_list) > 0:
            for goods_comment in goods_comments_list:
                comment_id = goods_comment.get('id')
                comment_user_id = goods_comment.get('user_id')
                comment_open_id = goods_comment.get('openid')
                comment_user_name = goods_comment.get('username')
                comment_content = goods_comment.get('content')
                comment_rank = goods_comment.get('rank')
                comment_add_time = goods_comment.get('add_time')
                comment_images = goods_comment.get('images')
                if comment_images is not None:
                    comment_images = json.dumps(comment_images)
                if comment_id not in exist_comment_ids:
                    insert_new_goods_comment(connection, comment_id, item, comment_user_id, comment_open_id,
                                             comment_user_name, comment_content, comment_rank, comment_images,
                                             comment_add_time)


def get_page_content(page_content, cate_id, cate_name, goods_id):
    record = []
    content_structure = page_content.replace('var jsonData = ', '').encode('utf-8')
    content = parse_js(content_structure.decode('unicode-escape'))
    gender = content.get('gender')
    gold_type = content.get('gold_type')
    goods_name = content.get('goods_name')
    goods_thumb = content.get('goods_thumb').replace('\\', '')
    market_price = content.get('market_price')
    material = content.get('material')
    sales_count = content.get('sales_count')
    shop_price = content.get('shop_price')
    stone = content.get('stone')
    stone_range = content.get('stone_range')
    return generate_goods_record(goods_id, goods_name, cate_id, cate_name, shop_price, market_price, goods_thumb,
                                 stone_range, sales_count, stone, material, gender, gold_type)


def generate_goods_record(goods_id, goods_name, cate_id, cate_name, shop_price, market_price, goods_thumb, stone_range,
                          sales_count, stone, material, gender, gold_type):
    insert_time = datetime.datetime.now()
    update_time = datetime.datetime.now()
    goods_recode = "'{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'".format(goods_id,
                                                                                                       goods_name,
                                                                                                       cate_id,
                                                                                                       cate_name,
                                                                                                       shop_price,
                                                                                                       market_price,
                                                                                                       goods_thumb,
                                                                                                       stone_range,
                                                                                                       sales_count,
                                                                                                       stone,
                                                                                                       material, gender,
                                                                                                       gold_type,
                                                                                                       insert_time,
                                                                                                       update_time)
    return goods_recode


if __name__ == '__main__':
    print "恋之钻更新开始... {}".format(datetime.datetime.now())
    # fileName = '{}_xinlangcaijing'.format(sys.argv[1])
    # errorfileName = sys.argv[2]

    conn = MySQLdb.connect(host='60.205.190.85', user='geek', passwd='tomcat', db="lian_zhi_zuan", charset="utf8")
    cursor = conn.cursor()
    saved_goods_id_list = connect_database(cursor)

    cate_info_url = 'http://www.lzzuan.com/api/cate.php?act=all_cat'
    cate_info = [
        {
            "cat_id": "1",
            "cat_name": "吊坠/项链",
            "goods_num": 674
        },
        {
            "cat_id": "126",
            "cat_name": "吊坠",
            "goods_num": 448
        },
        {
            "cat_id": "127",
            "cat_name": "项链",
            "goods_num": 194
        },
        {
            "cat_id": "3",
            "cat_name": "耳饰",
            "goods_num": 271
        },
        {
            "cat_id": "130",
            "cat_name": "耳坠",
            "goods_num": 78
        },
        {
            "cat_id": "129",
            "cat_name": "耳钉",
            "goods_num": 0
        },
        {
            "cat_id": "128",
            "cat_name": "耳环",
            "goods_num": 76
        },
        {
            "cat_id": "4",
            "cat_name": "裸石",
            "goods_num": 0
        },
        {
            "cat_id": "134",
            "cat_name": "祖母绿",
            "goods_num": 0
        },
        {
            "cat_id": "133",
            "cat_name": "红宝石",
            "goods_num": "10"
        },
        {
            "cat_id": "136",
            "cat_name": "碧玺",
            "goods_num": 0
        },
        {
            "cat_id": "135",
            "cat_name": "坦桑石",
            "goods_num": 0
        },
        {
            "cat_id": "131",
            "cat_name": "钻石",
            "goods_num": 0
        },
        {
            "cat_id": "132",
            "cat_name": "蓝宝石",
            "goods_num": 0
        },
        {
            "cat_id": "5",
            "cat_name": "翡翠/玉石",
            "goods_num": "7"
        },
        {
            "cat_id": "139",
            "cat_name": "玉石",
            "goods_num": 0
        },
        {
            "cat_id": "138",
            "cat_name": "翡翠",
            "goods_num": 0
        },
        {
            "cat_id": "27",
            "cat_name": "手链/手镯",
            "goods_num": 93
        },
        {
            "cat_id": "140",
            "cat_name": "手链",
            "goods_num": 60
        },
        {
            "cat_id": "141",
            "cat_name": "手镯",
            "goods_num": 7
        },
        {
            "cat_id": "39",
            "cat_name": "戒指",
            "goods_num": 472
        },
        {
            "cat_id": "124",
            "cat_name": "套戒",
            "goods_num": 0
        },
        {
            "cat_id": "121",
            "cat_name": "婚戒",
            "goods_num": 0
        },
        {
            "cat_id": "137",
            "cat_name": "对戒",
            "goods_num": 0
        },
        {
            "cat_id": "122",
            "cat_name": "女戒",
            "goods_num": 338
        },
        {
            "cat_id": "125",
            "cat_name": "素戒",
            "goods_num": 0
        },
        {
            "cat_id": "123",
            "cat_name": "男戒",
            "goods_num": 0
        },
        {
            "cat_id": "142",
            "cat_name": "其它",
            "goods_num": "2"
        }]

    for i in range(0, len(cate_info)):
        cat_id = cate_info[i].get('cat_id')
        cat_name = cate_info[i].get('cat_name')
        goods_id_list_url = 'http://www.lzzuan.com/api/cate.php?act=cat_goods&id={}'.format(cat_id)
        goods_id_list_html = get_url_info_by_urllib(goods_id_list_url, 'utf-8')
        goods_id_list = parse_js(goods_id_list_html)
        for j in goods_id_list:
            if len(saved_goods_id_list) > 0:
                if j in saved_goods_id_list:
                    update_goods_info(cursor, j)
                if j not in saved_goods_id_list:
                    # if j != saved_goods_id:
                    goods_info_url = 'http://www.lzzuan.com/api/cate.php?act=goods_info&id={}'.format(j)
                    goods_info_html = get_url_info_by_urllib(goods_info_url, 'utf-8')
                    goods_info = get_page_content(goods_info_html, cat_id, cat_name, j)
                    insert_new_goods_info(cursor, goods_info)
            if len(saved_goods_id_list) == 0:
                goods_info_url = 'http://www.lzzuan.com/api/cate.php?act=goods_info&id={}'.format(j)
                goods_info_html = get_url_info_by_urllib(goods_info_url, 'utf-8')
                goods_info = get_page_content(goods_info_html, cat_id, cat_name.encode('utf-8'), j)
                insert_new_goods_info(cursor, goods_info)
    get_good_comment(saved_goods_id_list, cursor)
    cursor.close()
    conn.close()
    print "恋之钻更新结束... {}".format(datetime.datetime.now())
