__author__ = 'elvis'
# -*- coding: utf-8 -*-

import traceback
import sys
import urllib2
import time
from bs4 import BeautifulSoup
import os

reload(sys)
sys.setdefaultencoding('utf8')


def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def read_xml(node, siteid):

    article_tags = node.find('category').text
    article_author = node.find('author').text
    article_cover = node.find('image').text
    article_title = node.find('title').text
    article_url = node.find('link').text
    article_description = node.find('description').text
    article_id = article_url.replace('http://www.01caijing.com/article/', '').replace('.htm', '')
    t = node.find('pubDate').text
    article_content = getContentPage(article_url)
    article_time = time_translator(t)

    article = getArticleRecord(article_id, article_title, article_author, article_content, article_tags, article_time,
                               siteid, article_url, article_cover, article_description)
    return article


def getContentPage(url):
    f = urllib2.urlopen(url, timeout=10)
    content = BeautifulSoup(f.read(), 'html.parser')
    article_content = content.find('div', attrs={'class': 'article-txt'}).text.replace('\n', '').replace('\r', '').strip()
    return article_content


def getArticleRecord(id, title, editor, content, tags, publishtime, siteid, url, cover, description):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,publishtime,siteid,url,cover,description)
    return article

def time_translator(t):
    print t
    timeArray = time.strptime(t, u"%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp

def stop_crawl_time_maker(timestamp, time_tag):
    timeArray = time.strptime(time_tag, u"%Y-%m-%d")
    stop_time_tag = int(time.mktime(timeArray))
    if float(timestamp) >= stop_time_tag:
        return False
    return True

if __name__=="__main__":
    pc_init_url = 'http://www.01caijing.com/rss/channel.htm?pageIndex={}'
    pc_siteid = '5043'
    if len(sys.argv) <= 3:
        fileName = sys.argv[1]
        errorfileName = sys.argv[2]
        completefile = str(sys.argv[1])+'.complete'
        for i in range(1, 300):
            f = urllib2.urlopen(pc_init_url.format(i), timeout=10)
            content = BeautifulSoup(f.read(), 'xml')
            for node in content.findAll('item'):
                try:
                    article = read_xml(node, pc_siteid)
                    # print article
                    writeDataSet(fileName, article)
                    # time.sleep(1)
                except Exception, e:
                    # exstr = traceback.format_exc()
                    error_record = str(node)+'\n'
                    # print error_record
                    writeDataSet(errorfileName, error_record)
                    pass
        os.rename(fileName, completefile)

    if len(sys.argv) == 4:
        exit_code = 0
        fileName = sys.argv[1]
        errorfileName = sys.argv[2]
        time_tag = sys.argv[3]
        completefile = str(sys.argv[1]) + '.complete'

        for i in range(1, 300):
            f = urllib2.urlopen(pc_init_url.format(i), timeout=10)
            content = BeautifulSoup(f.read(), 'xml')
            for node in content.findAll('item'):
                try:
                    if stop_crawl_time_maker(time_translator(node.find('pubDate').text), time_tag):
                        exit_code = 1
                        break
                    article = read_xml(node, pc_siteid)
                    # print article
                    writeDataSet(fileName, article)
                    # time.sleep(1)
                except Exception, e:
                    # exstr = traceback.format_exc()
                    # print exstr
                    error_record = str(node)+'\n'
                    # print error_record
                    writeDataSet(errorfileName, error_record)
                    pass
            if exit_code == 1:
                break
        os.rename(fileName, completefile)

