# -*- coding:utf-8 -*-
__author__ = 'elvis'
import urllib2
import time
import re
import json
import sys
import os
from bs4 import BeautifulSoup


def clean_data():
    result = set()
    with open(sys.argv[1], 'r') as f:
        data = f.readlines()
        for line in data:
            try:
                if '%' not in line:
                    line_split_html = line.split('.html')
                    line_split_slash = line_split_html[0].split('/')
                    art_num = line_split_slash[len(line_split_slash)-1][0:8]
                    new_line = '{}{}'.format(line_split_html[0].replace(line_split_slash[len(line_split_slash)-1], art_num), '_all.html')
                    result.add(new_line)
            except:
                pass
        return result


def save_data(result):
    for item in result:
        with open('/Users/elvis/Desktop/result', 'a') as nf:
            nf.write(item+'\n')


def write_data_set(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)


def get_url_info_by_urllib(init_url):
    fails = 0
    page_content = ''
    while True:
        if fails >= 5:
            break
        try:
            headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 "
                                     "(KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
                       }
            request = urllib2.Request(init_url, headers=headers)
            f = urllib2.urlopen(request, timeout=10)
            page_content = f.read()
            break
        except:
            time.sleep(3)
            fails += 1
    return page_content


def get_timestamp(str):
    pattern = re.compile(r'\d{4}-\d{2}-\d{2}\s+([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]')
    match = pattern.search(str)
    if match:
        timeArray = time.strptime(match.group(), u"%Y-%m-%d %H:%M:%S")
        timeStamp = int(time.mktime(timeArray))
        return timeStamp

def get_page_content(init_url, page_content):
    cover = []
    temp = ''
    content_structure = BeautifulSoup(page_content, 'html.parser')
    content = content_structure.find('div', attrs={'id': 'chan_newsBlk'})
    id = init_url.split('.html')[0].split('/')[-1].replace('_all', '')
    tags = init_url.split('/')[2].split('.')[0]
    title = content.h1.text.encode('utf-8')
    editor = ''
    publishtime = get_timestamp(content.find('div', attrs={'id': 'chan_newsInfo'}).text)
    for item in content.find_all('p'):
        detail = item.text
        temp += detail
    siteid = '5047'
    url = init_url.replace('_all', '')
    content_type = '0'
    try:
        for item in content.find_all('p', attrs={'class': re.compile(r'(\w+\s)pcenter|pcenter')}):
            if 'img' in str(item):
                cover.append(item.img.get('src'))
            if 0 < len(cover) < 3:
                content_type = '1'
            if len(cover) > 2:
                content_type = '2'
    except:
        pass
    cover = json.dumps(cover)

    record = get_article_record(id, title, editor, temp.encode('utf-8'), tags, publishtime, siteid, url,cover,content_type)
    return record


def get_article_record(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type):

    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id, title, editor, content, tags,
                                                                                  publishtime, siteid, url, cover,
                                                                                  content_type)
    return article


if __name__ == '__main__':
    output_file = sys.argv[2]
    complete_file = str(output_file) + '.complete'
    url_list = clean_data()
    for url in url_list:
        if 'china.com' in url.split('/')[2]:
            page = get_url_info_by_urllib(url)
            try:
                write_data = get_page_content(url, page)
                # print write_data
                write_data_set(output_file, write_data)
            except:
                pass
    os.rename(output_file, complete_file)
