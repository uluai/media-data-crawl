__author__ = 'elvis'
# -*- coding: utf-8 -*-

import traceback
import sys
import urllib2
import time
from bs4 import BeautifulSoup
import os
import json


reload(sys)
sys.setdefaultencoding('utf8')


def writeDataSet(fileName, data):
    fr = open(fileName,'a')
    fr.write(data)

def time_translator(t):
    timeArray = time.strptime(t, u"%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return timeStamp

def read_xml(node, siteid):
    base_url = 'http://cd.wenming.cn/'
    article_tags = node.find('category').text
    article_author = node.find('editor').text
    title_structure = BeautifulSoup(node.find('title').text, 'html.parser')
    article_title = title_structure.a.text
    article_url = '{}{}'.format(base_url, node.find('link').text.replace('../../', ''))
    content_structure = BeautifulSoup(node.find('description').text, 'html.parser')
    article_content = ''
    for item in content_structure.find_all('p'):
        article_content += item.text.replace('\n', '').replace('\r', '').replace(' ', '')
    cover_temp = []
    for item in content_structure.find_all('img'):
        cover_temp.append(item.get('src').replace('../../', ''))
    if len(cover_temp) == 0:
        content_type = '0'
    if 0 < len(cover_temp) < 3:
        content_type = '1'
    if len(cover_temp) > '2':
        content_type = '2'
    article_cover = json.dumps(cover_temp)
    article_id = article_url.split('_')[1].replace('.shtml', '')
    article_time = time_translator(node.find('pubDate').text)

    article = getArticleRecord(article_id, article_title, article_author, article_content, article_tags, article_time,
                               siteid, article_url, article_cover, content_type)
    return article


def getArticleRecord(id, title, editor, content, tags, publishtime, siteid, url, cover, content_type):
    article = '{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\001{}\n'.format(id,title,editor,content,tags,publishtime,siteid,url,cover, content_type)
    return article

if __name__=="__main__":
    # getContentPage('http://tech.gmw.cn/2017-05/04/content_24383622.htm')
    pc_init_url = ['http://cd.wenming.cn/rss/2016wmcd/index.xml',
                    'http://cd.wenming.cn/rss/2016wmcj/index.xml',
                    'http://cd.wenming.cn/rss/2016zyfw/index.xml',
                    'http://cd.wenming.cn/rss/2016wcnr/index.xml',
                    'http://cd.wenming.cn/rss/2016wmsx/index.xml',
                    'http://cd.wenming.cn/rss/2016rcrp/index.xml',
                    'http://cd.wenming.cn/rss/2016jcbw/index.xml',
                    'http://cd.wenming.cn/rss/2016bgt/index.xml',
                    'http://cd.wenming.cn/rss/2016byg/index.xml',
                    'http://cd.wenming.cn/rss/2016wmhd/index.xml']
    pc_siteid = '5050'
    fileName = sys.argv[1]
    errorfileName = sys.argv[2]
    completefile = str(sys.argv[1])+'.complete'
    for url in pc_init_url:
        # print url
        f = urllib2.urlopen(url, timeout=10)
        content = BeautifulSoup(f.read(), 'xml')
        for node in content.findAll('item'):
            try:
                article = read_xml(node, pc_siteid)
                # print article
                writeDataSet(fileName, article)
                # time.sleep(1)
            except Exception, e:
                # exstr = traceback.format_exc()
                # print exstr
                error_record = str(node)+'\n'
                # print error_record
                writeDataSet(errorfileName, error_record)
                pass
    os.rename(fileName, completefile)

